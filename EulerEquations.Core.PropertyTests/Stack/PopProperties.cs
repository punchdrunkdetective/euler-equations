﻿using System.Linq;
using FsCheck;
using FsCheck.NUnit;

using TestFixture = NUnit.Framework.TestFixtureAttribute;

namespace EulerEquations.Core.PropertyTests
{
    [TestFixture]
    public partial class StackProperties
    {
        [Property]
        public Property Pop_MaintainsFifoOrder()
        {
            var intGen = Gen.Choose(-100, 100);
            var stackGen = Gen.NonEmptyListOf(intGen); //empty lists undefined, check unit tests

            return Prop.ForAll(Arb.From(stackGen),
                stack =>
                {
                    var result = Stack.pop(stack);
                    var newStack = result.Item2;
                    var item = result.Item1;

                    return stack.Tail.SequenceEqual(newStack);
                });
        }
    }
}
