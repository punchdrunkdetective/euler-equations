﻿using System.Linq;
using FsCheck;
using FsCheck.NUnit;

using TestFixture = NUnit.Framework.TestFixtureAttribute;

namespace EulerEquations.Core.PropertyTests
{
    [TestFixture]
    public partial class StackProperties
    {
        [Property]
        public Property Push_MaintiansFifoOrder()
        {
            var itemGen = Gen.Choose(-100, 100);
            var stackGen = Gen.ListOf(itemGen);

            return Prop.ForAll(Arb.From(Gen.zip(stackGen, itemGen)),
                input =>
                {
                    var stack = input.Item1;
                    var item = input.Item2;
                    var newStack = Stack.push(stack, item);

                    return newStack.Tail.SequenceEqual(stack);
                });
        }
    }
}
