﻿using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class FloatTests
    {
        [TestCase(-7.3, -7.3)]
        [TestCase(-5.0, -5.0)]        
        [TestCase(0.0, 0.0)]
        [TestCase(5.0, 5.0)]
        [TestCase(7.3, 7.3)]
        public void Equals_EqualFloats_True(double f1, double f2)
        {
            Assert.IsTrue(Float.equals(tolerance, f1, f2));
        }

        [TestCase(-234.4865, -234.486968)]
        [TestCase(0.4861112, 0.4865)]
        [TestCase(234.4867, 234.4860505)]
        public void Equals_EqualByTolerance_True(double f1, double f2)
        {
            Assert.IsTrue(Float.equals(tolerance, f1, f2));
        }

        [TestCase(-14.67, -29.43)]
        [TestCase(14.67, 29.43)]
        public void Equals_UnEqualFloats_False(double f1, double f2)
        {
            Assert.IsFalse(Float.equals(tolerance, f1, f2));
        }

        [TestCase(-8392.155, -8392.156)]
        [TestCase(0.938, 0.939)]
        [TestCase(8392.155, 8392.156)]
        public void Equals_UnEqualByTolerance_False(double f1, double f2)
        {
            Assert.IsFalse(Float.equals(tolerance, f1, f2));
        }

        [TestCase(-38.49, 38.49)]
        [TestCase(38.49, -38.49)]
        public void Equals_UnEqualBySign_False(double f1, double f2)
        {
            Assert.IsFalse(Float.equals(tolerance, f1, f2));
        }
    }
}
