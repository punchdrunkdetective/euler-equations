﻿using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class FloatTests
    {
        [TestCase(3.2893, 183.02, 3.2893)]
        [TestCase(-3.2893, -183.02, -183.02)]
        [TestCase(3.2893, -183.02, -183.02)]
        [TestCase(-3.2893, 183.02, -3.2893)]
        public void Min_Floats_MinFloat(double f1, double f2, double expected)
        {
            Assert.AreEqual(expected, Float.min(tolerance, f1, f2), tolerance);
        }
    }
}
