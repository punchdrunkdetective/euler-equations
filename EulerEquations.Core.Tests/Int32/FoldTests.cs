﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int32Tests
    {
        [Test]
        public void Fold_NullList_ArgumentException()
        {
            FSharpList<int> input = null;
            Assert.Throws<ArgumentException>(() => Int32.fold(input));
        }

        [Test]
        public void Fold_EmptyList_ArgumentException()
        {
            var input = ListModule.OfSeq(new List<int>());
            Assert.Throws<ArgumentException>(() => Int32.fold(input));
        }

        [Test]
        public void Fold_Int64_OverflowException()
        {
            var input = ListModule.OfSeq(new List<int> { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, }); 
            Assert.Throws<OverflowException>(() => Int32.fold(input));
        }

        [Test]
        public void Fold_SingleDigit_Digit()
        {
            var input = ListModule.OfSeq(new List<int> { 5 });
            var expectedOutput = 5;

            var actualOutput = Int32.fold(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Fold_SingleNumber_Number()
        {
            var input = ListModule.OfSeq(new List<int> { 529003 });
            var expectedOutput = 529003;

            var actualOutput = Int32.fold(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Fold_ListOfDigits_Number()
        {
            var input = ListModule.OfSeq(new List<int> { 5, 2, 9, 0, 0, 3, });
            var expectedOutput = 529003;

            var actualOutput = Int32.fold(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Fold_ListOfNumbers_Number()
        {
            var input = ListModule.OfSeq(new List<int> { 52, 900, 3, });
            var expectedOutput = 529003;

            var actualOutput = Int32.fold(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
