﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int32Tests
    {
        [Test]
        public void Gcd_NullList_ArgumentException()
        {
            FSharpList<int> input = null;
            Assert.Throws<ArgumentException>(() => Int32.gcd(input));
        }

        [Test]
        public void Gcd_ListWithNegative_ArgumentException()
        {
            var input = ListModule.OfSeq(new List<int> { 605, 242, -1, 1089 });
            Assert.Throws<ArgumentException>(() => Int32.gcd(input));
        }

        [Test]
        public void Gcd_EmptyList_One()
        {
            var input = ListModule.OfSeq(new List<int>());
            var expectedGcd = 1;

            var actualGcd = Int32.gcd(input);

            Assert.AreEqual(expectedGcd, actualGcd);
        }

        [Test]
        public void Gcd_Zero_One()
        {
            var input = ListModule.OfSeq(new List<int> { 0 });
            var expectedGcd = 1;

            var actualGcd = Int32.gcd(input);

            Assert.AreEqual(expectedGcd, actualGcd);
        }

        [Test]
        public void Gcd_SingleNumber_SingleNumber()
        {
            var input = ListModule.OfSeq(new List<int> { 7 });
            var expectedGcd = 7;

            var actualGcd = Int32.gcd(input);

            Assert.AreEqual(expectedGcd, actualGcd);
        }

        [Test]
        public void Gcd_ListOfZeros_One()
        {
            var input = ListModule.OfSeq(new List<int> { 0, 0 });
            var expectedGcd = 1;

            var actualGcd = Int32.gcd(input);

            Assert.AreEqual(expectedGcd, actualGcd);
        }

        [Test]
        public void Gcd_ListOfPrimes_One()
        {
            var input = ListModule.OfSeq(new List<int> { 17, 3, 101, 131 });
            var expectedGcd = 1;

            var actualGcd = Int32.gcd(input);

            Assert.AreEqual(expectedGcd, actualGcd);
        }

        [Test]
        public void Gcd_ListWithCommonDivisor_Value()
        {
            var input = ListModule.OfSeq(new List<int> { 605, 242, 1089 });
            var expectedGcd = 121;

            var actualGcd = Int32.gcd(input);

            Assert.AreEqual(expectedGcd, actualGcd);
        }

    }
}
