﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int32Tests
    {
        [Test]
        public void Lcm_NullList_ArgumentException()
        {
            FSharpList<int> input = null;
            Assert.Throws<ArgumentException>(() => Int32.lcm(input));
        }

        [Test]
        public void Lcm_ListWithNegative_ArgumentException()
        {
            var input = ListModule.OfSeq(new List<int> { -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Assert.Throws<ArgumentException>(() => Int32.lcm(input));
        }

        [Test]
        public void Lcm_EmptyList_Zero()
        {
            var input = ListModule.OfSeq(new List<int>());
            var expectedLcm = 0;

            var actualLcm = Int32.lcm(input);

            Assert.AreEqual(expectedLcm, actualLcm);
        }

        [Test]
        public void Lcm_Zero_Zero()
        {
            var input = ListModule.OfSeq(new List<int> { 0 });
            var expectedLcm = 0;

            var actualLcm = Int32.lcm(input);

            Assert.AreEqual(expectedLcm, actualLcm);
        }

        [Test]
        public void Lcm_One_One()
        {
            var input = ListModule.OfSeq(new List<int> { 1 });
            var expectedLcm = 1;

            var actualLcm = Int32.lcm(input);

            Assert.AreEqual(expectedLcm, actualLcm);
        }

        [Test]
        public void Lcm_ListWithZero_Zero()
        {
            var input = ListModule.OfSeq(new List<int> { 1, 2, 10, 3, 5, 6, 0, 7, 4, 8, 9 });
            var expectedLcm = 0;

            var actualLcm = Int32.lcm(input);

            Assert.AreEqual(expectedLcm, actualLcm);
        }

        [Test]
        public void Lcm_List_Value()
        {
            var input = ListModule.OfSeq(new List<int> { 1, 2, 10, 3, 5, 6, 7, 4, 8, 9 });
            var expectedLcm = 2520;

            var actualLcm = Int32.lcm(input);

            Assert.AreEqual(expectedLcm, actualLcm);
        }
    }
}
