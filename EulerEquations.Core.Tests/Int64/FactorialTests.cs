﻿using System;
using System.Numerics;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [TestCase(-3L)]
        [TestCase(-1L)]
        public void Factorial_Negative_InvalidOperationException(long i)
        {
            Assert.Throws<InvalidOperationException>(() => Int64.factorial(i));
        }

        [Test]
        public void Factorial_Zero_One()
        {
            var input = 0L;
            var expectedOutput = new BigInteger(1);

            var actualOutput = Int64.factorial(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Factorial_One_One()
        {
            var input = 1L;
            var expectedOutput = new BigInteger(1);

            var actualOutput = Int64.factorial(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Factorial_Positive_Factorial()
        {
            var input = 10L;
            var expectedOutput = new BigInteger(3628800);

            var actualOutput = Int64.factorial(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
