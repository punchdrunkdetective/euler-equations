﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [Test]
        public void Factors_Negative_InvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => Int64.factors(-100L));
        }

        [Test]
        public void Factors_Zero_EmptyList()
        {
            var input = 0L;

            var factors = Int64.factors(input);

            Assert.IsTrue(factors.Length == 0, $"{factors.Length} factors returned.");
        }

        [Test]
        public void Factors_One_EmptyList()
        {
            var input = 1L;

            var factors = Int64.factors(input);

            Assert.IsTrue(factors.Length == 0, $"{factors.Length} factors returned.");
        }

        [Test]
        public void Factors_Two_EmptyList()
        {
            var input = 2L;

            var factors = Int64.factors(input);

            Assert.IsTrue(factors.Length == 0, $"{factors.Length} factors returned.");
        }

        [Test]
        public void Factors_Three_EmptyList()
        {
            var input = 3L;

            var factors = Int64.factors(input);

            Assert.IsTrue(factors.Length == 0, $"{factors.Length} factors returned.");
        }

        [Test]
        public void Factors_OneHundred_ListOfFactors()
        {
            var input = 100L;
            var expectedFactors = ListModule.OfSeq(new List<long> { 2, 4, 5, 10, 20, 25, 50 });

            var factors = Int64.factors(input);

            TestUtilities.AssertFSharpListsAreEqual(expectedFactors, factors);
        }

        [Test]
        public void Factors_Prime_EmptyList()
        {
            var input = 101L;

            var factors = Int64.factors(input);

            Assert.IsTrue(factors.Length == 0, $"{factors.Length} factors returned.");
        }
    }
}
