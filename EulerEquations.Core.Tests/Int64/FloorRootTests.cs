﻿using System;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [Test]
        public void FloorRoot_NegativeSquare_InvalidOperationException()
        {
            var square = -4L;
            Assert.Throws<InvalidOperationException>(() => Int64.floorRoot(square));
        }

        [Test]
        public void FloorRoot_NegativeNonSquare_InvalidOperationException()
        {
            var nonSquare = -5L;
            Assert.Throws<InvalidOperationException>(() => Int64.floorRoot(nonSquare));
        }

        [Test]
        public void FloorRoot_Zero_Zero()
        {
            var expectedRoot = 0L;

            var actualRoot = Int64.floorRoot(0L);

            Assert.AreEqual(expectedRoot, actualRoot);
        }

        [Test]
        public void FloorRoot_Square_Root()
        {
            var square = 4L;
            var expectedRoot = 2L;

            var actualRoot = Int64.floorRoot(square);

            Assert.AreEqual(expectedRoot, actualRoot);
        }

        [Test]
        public void FloorRoot_NonSquare_FloorRoot()
        {
            var nonSquare = 5L;
            var expectedFloor = 2L;

            var actualFloor = Int64.floorRoot(nonSquare);

            Assert.AreEqual(expectedFloor, actualFloor);
        }
    }
}
