﻿using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [TestCase(100L)]
        [TestCase(0L)]
        [TestCase(-100L)]
        public void IsEven_Even_True(long i)
        {
            Assert.IsTrue(Int64.isEven(i));
        }

        [TestCase(101L)]
        [TestCase(-101L)]
        public void IsEven_Odd_False(long i)
        {
            Assert.IsFalse(Int64.isEven(i));
        }
    }
}
