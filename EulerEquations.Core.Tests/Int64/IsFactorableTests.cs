﻿using System;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [Test]
        public void IsFactorable_Negative_InvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => Int64.isFactorable(-100L));
        }

        [Test]
        public void IsFactorable_Factorable_True()
        {
            var input = 100L;

            var success = Int64.isFactorable(input);

            Assert.IsTrue(success);
        }

        [Test]
        public void IsFactorable_NonFactorable_False()
        {
            var input = 101L;

            var success = Int64.isFactorable(input);

            Assert.IsFalse(success);
        }
    }
}
