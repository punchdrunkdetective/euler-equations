﻿using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [TestCase(101L)]
        [TestCase(-101L)]
        public void IsOdd_Odd_True(long i)
        {
            Assert.IsTrue(Int64.isOdd(i));
        }

        [TestCase(100L)]
        [TestCase(0L)]
        [TestCase(-100L)]
        public void IsOdd_Even_False(long i)
        {
            Assert.IsFalse(Int64.isOdd(i));
        }
    }
}
