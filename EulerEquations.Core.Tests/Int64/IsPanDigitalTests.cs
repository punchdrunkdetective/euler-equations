﻿using System;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [Test]
        public void IsPanDigital_NegativeNumber_ArgumentException()
        {
            var val = -576L;
            var from = 5;
            var to = 7;

            Assert.Throws<ArgumentException>(() => Int64.isPanDigital(from, to, val));
        }

        public void IsPanDigital_NegativeFrom_ArgumentException()
        {
            var val = 576L;
            var from = -5;
            var to = 7;

            Assert.Throws<ArgumentException>(() => Int64.isPanDigital(from, to, val));
        }

        [TestCase(576L, 5, -7)]
        [TestCase(576L, 7, 5)]
        public void IsPanDigital_DecreasingFromToRange_ArgumentException(long val, int from, int to)
        {
            Assert.Throws<ArgumentException>(() => Int64.isPanDigital(from, to, val));
        }

        [Test]
        public void IsPanDigital_RangeToLarge_ArgumentException()
        {
            var val = 576L;
            var from = 5;
            var to = 70;

            Assert.Throws<ArgumentException>(() => Int64.isPanDigital(from, to, val));
        }

        [TestCase(3L, 2, 3)]
        [TestCase(3L, 3, 4)]
        [TestCase(576L, 4, 7)]
        [TestCase(576L, 5, 8)]
        [TestCase(576L, 6, 7)]
        [TestCase(576L, 5, 6)]
        [TestCase(9768541203L, 1, 9)]
        [TestCase(9768541203L, 0, 8)]
        public void IsPanDigital_RangeOffByOne_False(long val, int from, int to)
        {
            Assert.IsFalse(Int64.isPanDigital(from, to, val));
        }

        [TestCase(3L, 3, 3)]
        [TestCase(576L, 5, 7)]
        [TestCase(9768541203L, 0, 9)]
        public void IsPanDigital_CorrectRange_True(long val, int from, int to)
        {
            Assert.IsTrue(Int64.isPanDigital(from, to, val));
        }

        [TestCase(764L, 4, 7)]
        [TestCase(754L, 4, 7)]
        [TestCase(968541203L, 0, 9)]
        [TestCase(976851203L, 0, 9)]
        public void IsPanDigital_MissingDigitsInRange_False(long val, int from, int to)
        {
            Assert.IsFalse(Int64.isPanDigital(from, to, val));
        }

        [TestCase(5776, 5, 7)]
        [TestCase(5576, 5, 7)]
        [TestCase(5766, 5, 7)]
        public void IsPanDigital_DuplicateDigits_False(long val, int from, int to)
        {
            Assert.IsFalse(Int64.isPanDigital(from, to, val));
        }
    }
}
