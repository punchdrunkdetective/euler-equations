﻿using Microsoft.FSharp.Collections;
using NUnit.Framework;
using System.Collections.Generic;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class Int64Tests
    {
        [Test]
        public void ToList_NegativeHundredthPlace_List()
        {
            var x = -342L;
            var expectedList = ListModule.OfSeq(new List<long> { -3L, -4L, -2L, });

            var actualList = Int64.toList(x);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void ToList_NegativeOnesPlace_List()
        {
            var x = -5L;
            var expectedList = ListModule.OfSeq(new List<long> { -5L, });

            var actualList = Int64.toList(x);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void ToList_Zero_List()
        {
            var x = 0L;
            var expectedList = ListModule.OfSeq(new List<long> { 0L, });

            var actualList = Int64.toList(x);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void ToList_OnesPlace_List()
        {
            var x = 8L;
            var expectedList = ListModule.OfSeq(new List<long> { 8L, });

            var actualList = Int64.toList(x);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void ToList_HundredthsPlace_List()
        {
            var x = 777L;
            var expectedList = ListModule.OfSeq(new List<long> { 7L, 7L, 7L, });

            var actualList = Int64.toList(x);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }
    }
}
