﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

using EulerEquations.Core;

namespace EulerEquationsTests
{
    [TestFixture]
    public partial class StackTests
    {
        [Test]
        public void Pop_EmptyStack_Exception()
        {
            var inStack = ListModule.OfSeq(new List<int>());
            Assert.Throws<Exception>(() => Stack.pop(inStack));
        }

        [Test]
        public void Pop_Stack_ItemPoped()
        {
            var inStack = ListModule.OfSeq(new List<int> { 1, 2, 3, 4, });
            var expectedOutItem = inStack[0];

            var actualOutItem = Stack.pop(inStack).Item1;

            Assert.AreEqual(expectedOutItem, actualOutItem);
        }
    }
}
