﻿using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

using EulerEquations.Core;

namespace EulerEquationsTests
{
    [TestFixture]
    public partial class StackTests
    {
        [Test]
        public void Push_Item_ItemPushed()
        {
            var inItem = 5;
            var inStack = ListModule.OfSeq(new List<int> { 1, 2, 3, 4 });

            var outStack = Stack.push(inStack, inItem);

            Assert.AreEqual(inItem, outStack[0]);
        }
    }
}
