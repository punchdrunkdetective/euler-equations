﻿using System;
using NUnit.Framework;

namespace EulerEquations.Core.Test
{
    [TestFixture]
    public partial class StringTests
    {
        [Test]
        public void Palindrome_Null_InvalidOperationException()
        {
            string s = null;
            Assert.Throws<InvalidOperationException>(() => String.isPalindrome(s));
        }

        [Test]
        public void Palindrome_EmptyString_True()
        {
            Assert.IsTrue(String.isPalindrome(""));
        }

        [TestCase("z")]
        [TestCase(" z ")]
        public void Palindrome_SingleLetter_True(string s)
        {
            Assert.IsTrue(String.isPalindrome(s));
        }

        [TestCase("zyyz")]
        [TestCase(" z yy z ")]
        public void Palindrome_ValidEvenLetters_True(string s)
        {
            Assert.IsTrue(String.isPalindrome(s));
        }

        [TestCase("zywyz")]
        [TestCase(" z y wyz  ")]
        public void Palindrome_ValidOddLetters_True(string s)
        {
            Assert.IsTrue(String.isPalindrome(s));
        }

        [TestCase("zywx")]
        [TestCase(" zyw x ")]
        public void Palindrome_InvalidEvenLetters_False(string s)
        {
            Assert.IsFalse(String.isPalindrome(s));
        }

        [TestCase("zywxx")]
        [TestCase(" z  ywxx ")]
        public void Palindrome_InvalidOddLetters_False(string s)
        {
            Assert.IsFalse(String.isPalindrome(s));
        }
    }
}
