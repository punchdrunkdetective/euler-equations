﻿namespace EulerEquations.Core

(*
* Type alias' for any .net types needed by core.
*)
[<AutoOpen>]
module DotNetTypes = 

    type BigInteger = System.Numerics.BigInteger

