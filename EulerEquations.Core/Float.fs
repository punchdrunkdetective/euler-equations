﻿namespace EulerEquations.Core

module Float = 

    let equals (tolerance :float) (f1 :float) (f2 :float) :bool = 
        (abs (f1 - f2)) <= tolerance

    let min (tolerance :float) (f1 :float) (f2 :float) :float = 
        match equals tolerance f1 f2 with 
        | true -> f1
        | false -> 
            match f1 > f2 with
            | true -> f2
            | false -> f1
