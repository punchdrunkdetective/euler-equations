﻿namespace EulerEquations.Core

[<System.Runtime.CompilerServices.Extension>]
module FloatExtensions =

    [<System.Runtime.CompilerServices.Extension>]
    let equals (f1 :System.Double) (f2 :System.Double) (tolerance :System.Double) :System.Boolean = 
        Float.equals tolerance f1 f2

    [<System.Runtime.CompilerServices.Extension>]
    let min (f1 :System.Double) (f2 :System.Double) (tolerance :System.Double) :System.Double= 
        Float.min tolerance f1 f2