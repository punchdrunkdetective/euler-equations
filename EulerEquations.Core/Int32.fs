﻿namespace EulerEquations.Core

module Int32 = 

    let private isPositive x = 
        match x < 0 with 
        | true -> Error (sprintf "negative value: %i" x)
        | false -> Ok x

    let private isOk res1 res2 = 
        match (res1, res2) with
        | (Ok _, Ok _) -> res2
        | (Error _, _) -> res1
        | (_, Error _) -> res2

    let private allPositive l = 
        l
        |> List.map isPositive
        |> List.fold isOk (Ok 0)
        |> Result.map (fun _ -> l)

    let private nonNullList l = 
        match isNull (box l) with 
        | true -> Error "null reference of list"
        | false -> Ok l

    let private nonEmptyList l = 
        match List.isEmpty l with 
        | true -> Error "empty list"
        | false -> Ok l

    (* 
    * Gives you the greatest common divisor from the given list of numbers. The gcd of two 0s
    * is considered to be 1, so this method will never return 0.
    *)
    let gcd (l :int32 list) :int32 =
        
        let validInput = 
            (Ok l) 
            |> Result.bind nonNullList
            |> Result.bind allPositive

        match validInput with 
        | Error e-> invalidArg "" e
        | Ok _->         

            let rec gcdHelper a b = 
                match (a, b) with 
                | (0, 0) -> 1
                | (a, 0) -> a
                | _ -> gcdHelper b (a % b)

            match l with
            | [] -> 1
            | 0 :: [] -> 1
            | x :: [] -> x
            | _ -> l |> List.reduce gcdHelper

    (* 
    * Gives you the least common multiple in the given list of numbers. 
    *)
    let lcm (l :int32 list) :int32 = 

        let validInput = 
            (Ok l) 
            |> Result.bind nonNullList
            |> Result.bind allPositive

        match validInput with 
        | Error e -> invalidArg "" e
        | Ok _ ->  

            let lcmHelper a b = (a / (gcd [a; b])) * b

            match l with 
            | [] -> 0
            | x :: [] -> x
            | _ -> l |> List.reduce lcmHelper

    (*
    * Converts a list of digits into a int64 by concatinating them in the order given.
    *)
    let fold (l :int32 list) :int32 =

        let validInput = 
            (Ok l) 
            |> Result.bind nonNullList
            |> Result.bind nonEmptyList

        match validInput with 
        | Error e -> invalidArg "" e
        | _ -> 
                       
            let digits = 
                l 
                |> List.collect (int64 >> Int64.toList) // TODO: make a Int32.toList
                |> List.map int32

            let placeMulipliers = 
                List.init (List.length digits) id 
                |> List.map (pown 10) 
                |> List.rev

            digits
                |> List.map2 (*) placeMulipliers
                |> List.sum

