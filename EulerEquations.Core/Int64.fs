namespace EulerEquations.Core

//TODO: make validation methods, alot of these methods share the same kind of input validation

module Int64 =

    (*
    * Determines if x is even or not.
    *)
    let isEven (i :int64) :bool = i % 2L = 0L

    (*
    * Determines if x is odd or not.
    *)
    let isOdd (i :int64) :bool = (not << isEven) i

    (*
    * Returns the floor of the square root of x. 
    * x should not be negative, library does not handle imaginary numbers.
    *)
    let floorRoot (i :int64) :int64 =
        match i < 0L with 
        | true -> invalidOp (sprintf "sqrt of negative") //TODO: perhaps this should be a maybe type
        | false -> int64 (floor (sqrt (double i)))  

    (*
    * Returns the factorial of x.
    *
    * O(n) where n = x
    *)
    let factorial (num :int64) :BigInteger =
        match num with
        | num when num < 0L -> invalidOp (sprintf "negative domain is undefined: %i" num)
        | 0L -> (BigInteger(1L))
        | 1L -> (BigInteger(1L))
        | _ -> 
            let rec build i output = 
                match i > num with
                | true -> output
                | false -> build (i + 1L) (BigInteger.Multiply(BigInteger(i), output)) // recursive step: i * (i - 1) * (i - 2)!
            build 1L (BigInteger(1L))

    (*
    * Converts x into a list of it's digits.
    *
    * O(n) where n is the number of digits in x.
    *)
    let toList (i :int64) :int64 list = 
        match i = 0L with 
        | true -> [0L]
        | false ->
            let rec parse i output = 
                match i with 
                | 0L -> output
                | i -> parse (i / 10L) (i % 10L :: output) 
            parse i [] 

    (* 
    * Returns all non trivial factors of x. (All factors not x itself or one.) 
    * If there are no factors, an empty list will be returned. x should not be negative.
    *
    * O(sqrt n) where n = x
    *)
    let factors (num :int64) :int64 list =

            let isFactor x factor = (x % factor) = 0L 
            let otherFactor x factor = x / factor
            let notSquareFactor x factor = (x / factor) <> factor  
            let concat l i = i :: l

            match num < 0L with 
            | true -> invalidOp (sprintf "negative value")
            | false -> 

                match num with 
                | 0L | 1L | 2L | 3L -> []
                | _ -> 
                    
                    let factors = 
                        [2L..(floorRoot num)] // You only have to consider up to the sqrt x for factor canidates.
                        |> List.filter (isFactor num)

                    factors
                    |> List.map (otherFactor num)
                    |> List.filter (notSquareFactor num)
                    |> List.rev
                    |> List.fold concat (List.rev factors) 
                    |> List.rev

    (* 
    * Determines if x is factorable. 
    * x should not be negative.
    * 
    * O(sqrt n) where n = x
    *)
    let isFactorable (i :int64) :bool =
        factors i |> List.isEmpty = false

    (*
    * Determines if x if pandigital between 'from' and 'to' values. Values of 'from' and 'to' need to 
    * be single digit values and match the condition 'from' <= 'to'. x should not be negative.
    * 
    * From Wikipedia: A pandigital number is an integer that in a given base has among its significant 
    * digits each digit used in the base at least once.
    *
    * This method differs by that by considering just the digits specified by a range of significant 
    * digits in base 10 and each digit must be used exactly once.
    *)
    let isPanDigital (from :int32) (``to`` :int32) (num :int64) :bool = 
    
        let isPositive x = match x < 0L with true -> Error (sprintf "negative domain is undefined: %i" x) | false -> Ok x 
        let isDigit x = match x > 9L with true -> Error (sprintf "domain is undefined for values > 9: %i" x) | false -> Ok x 
        let isValidRange (low, high) = match low > high with true -> Error (sprintf "invalid range given. from: %i to: %i" low high) | false -> Ok (low, high) 

        let isValidInput =
            Ok (num, from, ``to``)
            |> Result.map (fun _ -> num)
            |> Result.bind isPositive
            |> Result.map (fun _ -> from |> int64)
            |> Result.bind isPositive
            |> Result.map (fun _ -> ``to`` |> int64)
            |> Result.bind isPositive
            |> Result.bind isDigit
            |> Result.map (fun _ -> (from, ``to``))
            |> Result.bind isValidRange
            |> Result.map (fun _ -> (num, from, ``to``))

        match isValidInput with 
        | Error e -> invalidArg "" e
        | Ok _ -> 
                
            let digits = 
                num
                |> toList 
                |> List.map int32

            let noDuplicates l = (l |> List.length) = (l |> List.distinct |> List.length)
            let inRange l = l |> List.forall (fun i -> from <= i && i <= ``to``)
            let allDigitsUsed l = (l |> List.length) = ([from..``to``] |> List.length)

            List.fold 
                (fun f1 f2 x -> f1 x && f2 x) 
                (fun _ -> true) 
                [noDuplicates; inRange; allDigitsUsed]
                digits



