﻿namespace EulerEquations.Core

[<System.Runtime.CompilerServices.Extension>]
module Int64Extensiions =

    [<System.Runtime.CompilerServices.Extension>]
    let isEven (x :System.Int64) :bool =
        Int64.isEven x

    [<System.Runtime.CompilerServices.Extension>]
    let isOdd (x :System.Int64) :bool = 
        Int64.isOdd x

    [<System.Runtime.CompilerServices.Extension>]
    let floorRoot (x :System.Int64) :System.Int64 =
        Int64.floorRoot x

    [<System.Runtime.CompilerServices.Extension>]
    let factorial (x :System.Int64) :BigInteger = 
        Int64.factorial x

    [<System.Runtime.CompilerServices.Extension>]
    let factors (x :System.Int64) :int64 list = 
        Int64.factors x

    [<System.Runtime.CompilerServices.Extension>]
    let isFactorable (x :System.Int64) :bool =
        Int64.isFactorable x

    [<System.Runtime.CompilerServices.Extension>]
    let toList (x :System.Int64) :System.Int64 list = 
        Int64.toList x

    [<System.Runtime.CompilerServices.Extension>]
    let isPanDigital (x :System.Int64) (from :int32) (``to`` :int32) :bool =
        Int64.isPanDigital from ``to`` x