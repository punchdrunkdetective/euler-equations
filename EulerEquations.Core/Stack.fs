﻿namespace EulerEquations.Core

module Stack = 

    let push (l :'T list) (x :'T) :'T list  = 
        x :: l

    let pop (l :'T list) :('T * 'T list) =  
        match l with 
        | [] -> failwith "pop called on empty stack."
        | h :: t -> (h, t)
