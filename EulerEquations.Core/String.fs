﻿namespace EulerEquations.Core

module String =

    (* 
    * Determines if the given input is a plaindrome or not. Whitespace is accounted for. 
    *)
    let isPalindrome (s :string) :bool = 
    
        match s with 
        | null -> invalidOp "null reference"
        | _  -> 

            let isChar c = c <> ' '

            let chars = s.ToCharArray() |> List.ofArray |> List.filter isChar
            let revChars = chars |> List.rev

            List.forall2 (=) chars revChars
