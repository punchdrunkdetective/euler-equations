﻿namespace EulerEquations.Core

[<System.Runtime.CompilerServices.Extension>]
module StringExtensions = 

    [<System.Runtime.CompilerServices.Extension>]
    let isPalindrome (s :System.String) :System.Boolean =
        String.isPalindrome s