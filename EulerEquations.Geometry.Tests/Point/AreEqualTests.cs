﻿using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class PointTests
    {
        [Test]
        public void AreEqual_EqualPoints_True()
        {
            var p1 = Point.create(7.0, 13.0);
            var p2 = Point.create(7.0, 13.0);

            Assert.IsTrue(Point.areEqual(p1, p2));
        }

        [TestCase(7.0, 13.0, 26.0, 4.0)]
        [TestCase(7.0, 13.0, 7.0, 4.0)]
        [TestCase(7.0, 13.0, 26.0, 13.0)]
        public void AreEqual_UnequalPoints_False(double x1, double y1, double x2, double y2)
        {
            var p1 = Point.create(x1, y1);
            var p2 = Point.create(x2, y2);

            Assert.IsFalse(Point.areEqual(p1, p2));
        }
    }
}
