﻿using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class PointTests
    {
        [TestCase(0.0, 0.0)]
        [TestCase(0.7, 0.13)]
        [TestCase(-0.7, -0.13)]
        [TestCase(0.7, -0.13)]
        [TestCase(-0.7, 0.13)]
        public void Create_Point(double x, double y)
        {
            var p = Point.create(x, y);
            Assert.AreEqual(x, p.Item1, "Unexpected x value.");
            Assert.AreEqual(y, p.Item2, "Unexpected y value.");
        }
    }
}
