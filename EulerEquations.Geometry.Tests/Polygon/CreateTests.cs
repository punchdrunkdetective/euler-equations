﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class PolygonTests
    {
        [Test]
        public void Create_ThreePoints_Polygon()
        {
            var points =
                ListModule.OfSeq(new List<Point.T> {
                    Point.create(16.0, 7.0),
                    Point.create(23.0, 24.0),
                    Point.create(35.0, 18.0),
                });

            var poly = Polygon.create(points);

            AssertPointsAreEqual(points, poly.Item);
        }

        [Test]
        public void Create_LotsOfPoints_Polygon()
        {
            var poly = Polygon.create(testPolyPoints);

            AssertPointsAreEqual(testPolyPoints, poly.Item);
        }

        [Test]
        public void Create_NoPoints_ArgumentException()
        {
            var points = FSharpList<Point.T>.Empty;

            Assert.Throws<ArgumentException>(() => Polygon.create(points));
        }

        [Test]
        public void Create_OnePoint_ArguementException()
        {
            var points =
                ListModule.OfSeq(new List<Point.T> {
                    Point.create(16.0, 7.0),
                });

            Assert.Throws<ArgumentException>(() => Polygon.create(points));
        }

        [Test]
        public void Create_TwoPoints_ArguementException()
        {
            var points =
                ListModule.OfSeq(new List<Point.T> {
                    Point.create(16.0, 7.0),
                    Point.create(23.0, 24.0),
                });

            Assert.Throws<ArgumentException>(() => Polygon.create(points));
        }

        [Test]
        public void Create_RepeatingPoints_ArgumentException()
        {
            var points =
                ListModule.OfSeq(new List<Point.T> {
                    Point.create(16.0, 7.0),  
                    Point.create(16.0, 13.0), // repeats
                    Point.create(23.0, 24.0), 
                    Point.create(35.0, 18.0),
                    Point.create(16.0, 13.0), // repeats
                    Point.create(40.0, 20.0),
                    Point.create(42.0, 11.0), 
                    Point.create(37.0, 3.0),
                });

            Assert.Throws<ArgumentException>(() => Polygon.create(points));
        }

        private void AssertPointsAreEqual(FSharpList<Point.T> l1, FSharpList<Point.T> l2)
        {
            Assert.AreEqual(l1.Length, l1.Length, "Unexpected list size.");
            for (var i = 0; i < l1.Length; i++)
            {
                AssertPointsAreEqual(l1[i], l2[i]);
            }
        }

        private void AssertPointsAreEqual(Point.T p1, Point.T p2)
        {
            Assert.IsTrue(Point.areEqual(p1, p2));
        }
    }
}
