﻿using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class PolygonTests
    {
        private static object[] IsPointInPolygon_TestCases =
        {
            new TestCaseData(Point.create(31.0, 15.0))
                .Returns(true)
                .SetName(nameof(IsPointInPolygon_) + "InsidePolygon_True"),

            new TestCaseData(Point.create(33.0, 22.0))
                .Returns(false)
                .SetName(nameof(IsPointInPolygon_) + "OutsidePolygon_False"),

            new TestCaseData(Point.create(29.0, 21.0))
                .Returns(true)
                .SetName(nameof(IsPointInPolygon_) + "OnPolygonSegment_True"),

            new TestCaseData(Point.create(23.0, 24.0))
                .Returns(true)
                .SetName(nameof(IsPointInPolygon_) + "OnPolygonVertex_True"),

            new TestCaseData(Point.create(28.0, 7.0))
                .Returns(true)
                .SetName(nameof(IsPointInPolygon_) + "InsidePolygon_RayCastThroughSegment_True"),

            new TestCaseData(Point.create(30.0, 2.0))
                .Returns(false)
                .SetName(nameof(IsPointInPolygon_) + "OutsidePolygon_RayCastThroughSegment_False"),

            new TestCaseData(Point.create(34.0, 11.0))
                .Returns(true)
                .SetName(nameof(IsPointInPolygon_) + "InsidePolygon_RayCastThroughVertex_True"),

            new TestCaseData(Point.create(26.0, 24.0))
                .Returns(false)
                .SetName(nameof(IsPointInPolygon_) + "OutsidePolygon_RayCastThroughVertex_False"),
        };

        [TestCaseSource(nameof(IsPointInPolygon_TestCases))]
        public bool IsPointInPolygon_(Point.T testPoint)
        {
            return Polygon.isPointInPolygon(testPoint, testPoly);
        }
    }
}
