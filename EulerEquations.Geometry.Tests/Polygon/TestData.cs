﻿using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class PolygonTests
    {
        private static FSharpList<Point.T> testPolyPoints = 
            ListModule.OfSeq(new List<Point.T> {
                    Point.create(16.0, 7.0),  // farthest point to the left
                    Point.create(16.0, 13.0),
                    Point.create(23.0, 24.0), // farthest point up
                    Point.create(35.0, 18.0),
                    Point.create(40.0, 20.0),
                    Point.create(42.0, 11.0), // farthest point to the right
                    Point.create(37.0, 3.0),
                    Point.create(33.0, 10.0),
                    Point.create(34.0, 2.0),  // farthest point down
                    Point.create(31.0, 2.0),
                    Point.create(26.0, 5.0),
                    Point.create(28.0, 8.0),
                    Point.create(20.0, 7.0),
                });

        private static Polygon.T testPoly =
            Polygon.create(testPolyPoints);
    }
}
