﻿using System;
using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class SegmentTests
    {
        [Test]
        public void Create_Segment()
        {
            var p1 = Point.create(-7.0, -13.0);
            var p2 = Point.create(7.0, 13.0);

            var seg = Segment.create(p1, p2);            
            
            AssertPointsAreEqual(p1, seg.Item1);
            AssertPointsAreEqual(p2, seg.Item2);
        }

        [Test]
        public void Create_EqualPoints_ArgumentException()
        {
            var p1 = Point.create(7.0, 13.0);
            var p2 = Point.create(7.0, 13.0);

            Assert.Throws<ArgumentException>(() => Segment.create(p1, p2));
        }

        private void AssertPointsAreEqual(Point.T p1, Point.T p2)
        {
            Assert.IsTrue(Point.areEqual(p1, p2));
        }
    }
}
