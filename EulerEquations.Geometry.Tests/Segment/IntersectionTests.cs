﻿using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class SegmentTests
    {
        private static object[] WithPerpendicularSegments_TestCases =
         {
            new TestCaseData(
                Segment.create( // vertical  segment
                    Point.create(6.0, 4.0),
                    Point.create(6.0, 10.0)
                ),
                Segment.create( // horizontal segment
                    Point.create(3.0, 8.0),
                    Point.create(9.0, 8.0)
                ),
                Point.create(6.0, 8.0)).SetName(nameof(Intersection_PerpendicularSegments_IntersectionPoint)),

            new TestCaseData(
                Segment.create( // horizontal segment
                    Point.create(3.0, 8.0),
                    Point.create(9.0, 8.0)
                ),
                Segment.create( // vertical  segment
                    Point.create(6.0, 4.0),
                    Point.create(6.0, 10.0)
                ),
                Point.create(6.0, 8.0)).SetName(nameof(Intersection_PerpendicularSegments_IntersectionPoint) + "_Inverted"),
        };

        private static object[] WithVerticalSegment_TestCases =
         {
            new TestCaseData(
                Segment.create( // vertical  segment
                    Point.create(6.0, 4.0),
                    Point.create(6.0, 10.0)
                ),
                Segment.create( // diagonal segment
                    Point.create(2.0, 12.0),
                    Point.create(10.0, 2.0)
                ),
                Point.create(6.0, 7.0)).SetName(nameof(Intersection_WithVerticalSegment_IntersectionPoint)),

            new TestCaseData(
                Segment.create( // diagonal segment
                    Point.create(2.0, 12.0),
                    Point.create(10.0, 2.0)
                ),
                Segment.create( // vertical  segment
                    Point.create(6.0, 4.0),
                    Point.create(6.0, 10.0)
                ),
                Point.create(6.0, 7.0)).SetName(nameof(Intersection_WithVerticalSegment_IntersectionPoint) + "_Inverted"),
        };

        private static object[] WithHorizontalSegment_TestCases =
        {
            new TestCaseData(
                Segment.create( // horizontal segment
                    Point.create(3.0, 7.0),
                    Point.create(9.0, 7.0)
                ),
                Segment.create( // diagonal segment
                    Point.create(2.0, 12.0),
                    Point.create(10.0, 2.0)
                ),
                Point.create(6.0, 7.0)).SetName(nameof(Intersection_WithHorizontalSegment_IntersectionPoint)),

            new TestCaseData(
                Segment.create( // diagonal segment
                    Point.create(2.0, 12.0),
                    Point.create(10.0, 2.0)
                ),
                Segment.create( // horizontal segment
                    Point.create(3.0, 7.0),
                    Point.create(9.0, 7.0)
                ),
                Point.create(6.0, 7.0)).SetName(nameof(Intersection_WithHorizontalSegment_IntersectionPoint) + "_Inverted"),
        };

        [Test]
        public void Intersetion_CrossingSegments_IntersectionPoint()
        {
            var seg1 = Segment.create(
                Point.create(3.0, 5.0),
                Point.create(9.0, 9.0)
            );
            var seg2 = Segment.create(
                Point.create(2.0, 12.0),
                Point.create(10.0, 2.0)
            );
            var expectedIntersection = Point.create(6.0, 7.0);

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsCross, "Unexpected intersection type returned.");
            switch (actualIntersection)
            {
                case Segment.Intersection.Cross intr:
                    var actual = intr.Item;
                    Assert.AreEqual(expectedIntersection.Item1, actual.Item1, Constants.tolerance, "Unexpected x value.");
                    Assert.AreEqual(expectedIntersection.Item2, actual.Item2, Constants.tolerance, "Unexpected y value.");
                    break;
                default:
                    Assert.Fail("Unable to unpack intersection.");
                    break;
            }
        }

        [Test]
        public void Intersection_NonCrossingSegments_NoIntersectionPoint()
        {
            var seg1 = Segment.create(
                Point.create(3.0, 5.0),
                Point.create(9.0, 9.0)
            );
            var seg2 = Segment.create(
                Point.create(2.0, 10.0),
                Point.create(5.0, 8.0)
            );

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsNone, "Unexpected intersection type returned.");
        }

        [Test]
        public void Intersection_NonCrossingSegmentsOutsideBounds_NoIntersectionPoint()
        {
            // segments intersect outside the segements combined domain and range
            var seg1 = Segment.create(
                Point.create(3.0, 5.0),
                Point.create(9.0, 9.0)
            );
            var seg2 = Segment.create(
                Point.create(4.0, 4.0),
                Point.create(8.0, 5.0)
            );

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsNone, "Unexpected intersection type returned.");
        }
 
        [TestCaseSource(nameof(WithPerpendicularSegments_TestCases))]
        public void Intersection_PerpendicularSegments_IntersectionPoint(Segment.T seg1, Segment.T seg2, Point.T expectedIntersection)
        {
            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsCross, "Unexpected intersection type returned.");
            switch (actualIntersection)
            {
                case Segment.Intersection.Cross intr:
                    var actual = intr.Item;
                    Assert.AreEqual(expectedIntersection.Item1, actual.Item1, Constants.tolerance, "Unexpected x value.");
                    Assert.AreEqual(expectedIntersection.Item2, actual.Item2, Constants.tolerance, "Unexpected y value.");
                    break;
                default:
                    Assert.Fail("Unable to unpack intersection.");
                    break;
            }
        }

        [Test]
        public void Intersection_DiagonalParallelSegements_NoIntersectionPoint()
        {
            var seg1 = Segment.create(
                Point.create(3.0, 5.0),
                Point.create(9.0, 9.0)
            );
            var seg2 = Segment.create(
                Point.create(6.0, 9.0),
                Point.create(12.0, 13.0)
            );

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsParrallel, "Unexpected intersection type returned.");
        }

        [Test]
        public void Intersection_VerticalParallelSegements_NoIntersectionPoint()
        {
            var seg1 = Segment.create(
                Point.create(3.0, 5.0),
                Point.create(3.0, 10.0)
            );
            var seg2 = Segment.create(
                Point.create(6.0, 8.0),
                Point.create(6.0, 13.0)
            );

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsParrallel, "Unexpected intersection type returned.");
        }

        [Test]
        public void Intersection_HorizontalParallelSegements_NoIntersectionPoint()
        {
            var seg1 = Segment.create(
                Point.create(3.0, 9.0),
                Point.create(9.0, 9.0)
            );
            var seg2 = Segment.create(
                Point.create(6.0, 13.0),
                Point.create(12.0, 13.0)
            );

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsParrallel, "Unexpected intersection type returned.");
        }

        [Test]
        public void Intersection_OverlapingSegments_NoIntersectionPoint()
        {
            //TODO: determinen some non intersecting segments
            var seg1 = Segment.create(
                Point.create(3.0, 5.0),
                Point.create(9.0, 9.0)
            );
            var seg2 = Segment.create(
                Point.create(6.0, 7.0),
                Point.create(12.0, 11.0)
            );

            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsParrallel, "Unexpected intersection type returned.");
        }

        [TestCaseSource(nameof(WithVerticalSegment_TestCases))]
        public void Intersection_WithVerticalSegment_IntersectionPoint(Segment.T seg1, Segment.T seg2, Point.T expectedIntersection)
        {
            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsCross, "Unexpected intersection type returned.");
            switch (actualIntersection)
            {
                case Segment.Intersection.Cross intr:
                    var actual = intr.Item;
                    Assert.AreEqual(expectedIntersection.Item1, actual.Item1, "Unexpected x value.");
                    Assert.AreEqual(expectedIntersection.Item2, actual.Item2, "Unexpected y value.");
                    break;
                default:
                    Assert.Fail("Unable to unpack intersection.");
                    break;
            }
        }

        [TestCaseSource(nameof(WithHorizontalSegment_TestCases))]
        public void Intersection_WithHorizontalSegment_IntersectionPoint(Segment.T seg1, Segment.T seg2, Point.T expectedIntersection)
        {
            var actualIntersection = Segment.intersection(seg1, seg2);

            Assert.IsTrue(actualIntersection.IsCross, "Unexpected intersection type returned.");
            switch (actualIntersection)
            {
                case Segment.Intersection.Cross intr:
                    var actual = intr.Item;
                    Assert.AreEqual(expectedIntersection.Item1, actual.Item1, "Unexpected x value.");
                    Assert.AreEqual(expectedIntersection.Item2, actual.Item2, "Unexpected y value.");
                    break;
                default:
                    Assert.Fail("Unable to unpack intersection.");
                    break;
            }
        }
    }
}
