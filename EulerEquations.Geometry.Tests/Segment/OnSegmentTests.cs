﻿using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class SegmentTests
    {
        private static Segment.T _testSeg =>
            Segment.create( // Has slope of 2/3.
                Point.create(3.0, 5.0),
                Point.create(9.0, 9.0));

        private static object[] OnSegment_PointOffSegment_TestCases =
        {
            new TestCaseData(Point.create(4.0, 8.0)).Returns(false).SetName(nameof(OnSegment_PointOffSegment) + "_OffSlope_False"),
            new TestCaseData(Point.create(12.0, 11.0)).Returns(false).SetName(nameof(OnSegment_PointOffSegment) + "_OnSlope_False"),
        };

        private static object[] OnSegment_PointOnSegmentVertex_TestCases =
        {
            new TestCaseData(_testSeg.Item1).Returns(true).SetName(nameof(OnSegment_PointOnSegmentVertex) + "_Point1_True"),
            new TestCaseData(_testSeg.Item2).Returns(true).SetName(nameof(OnSegment_PointOnSegmentVertex) + "_Point2_True"),
        };

        [Test]
        public void OnSegment_PointOnSegemnt_True()
        {
            var p = Point.create(6.0, 7.0);
            var output = Segment.onSegment(p, _testSeg);
            Assert.IsTrue(output);
        }

        [TestCaseSource(nameof(OnSegment_PointOffSegment_TestCases))]
        public bool OnSegment_PointOffSegment(Point.T point)
        {
            return Segment.onSegment(point, _testSeg);
        }

        [TestCaseSource(nameof(OnSegment_PointOnSegmentVertex_TestCases))]
        public bool OnSegment_PointOnSegmentVertex(Point.T point)
        {
            return Segment.onSegment(point, _testSeg);
        }

        [Test]
        public void OnSegment_VerticalSegment_PointOnSegment_True()
        {
            var seg = Segment.create( 
                Point.create(7.0, 2.0),
                Point.create(7.0, 5.0));
            var point = Point.create(7.0, 3.0);

            var output = Segment.onSegment(point, seg);

            Assert.IsTrue(output);
        }

        [Test]
        public void OnSegment_VerticalSegment_PointOffSegment_False()
        {
            var seg = Segment.create(
                Point.create(7.0, 2.0),
                Point.create(7.0, 5.0));
            var point = Point.create(7.0, 10.0);

            var output = Segment.onSegment(point, seg);

            Assert.IsFalse(output);
        }

        [Test]
        public void OnSegment_HorizontalSegment_PointOnSegment_True()
        {
            var seg = Segment.create(
                Point.create(4.0, 3.0),
                Point.create(8.0, 3.0));
            var point = Point.create(5.0, 3.0);

            var output = Segment.onSegment(point, seg);

            Assert.IsTrue(output);
        }

        [Test]
        public void OnSegment_HorizontaSegment_PointOffSegment_False()
        {
            var seg = Segment.create(
                Point.create(4.0, 3.0),
                Point.create(8.0, 3.0));
            var point = Point.create(10.0, 3.0);

            var output = Segment.onSegment(point, seg);

            Assert.IsFalse(output);
        }
    }
}
