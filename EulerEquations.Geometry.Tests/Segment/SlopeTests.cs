﻿using NUnit.Framework;

namespace EulerEquations.Geometry.Tests
{
    [TestFixture]
    public partial class SegmentTests
    {
        [Test]
        public void Slope_InclineSegment_PositiveSlope()
        {
            var seg =
                Segment.create(
                    Point.create(10.0, 12.0),
                    Point.create(2.0, 2.0));
            var expectedSlope = 10.0 / 8.0;

            var actualSlope = Segment.slope(seg);

            Assert.IsTrue(actualSlope.IsDiagonal, "Unexpected slope type returned.");
            switch (actualSlope)
            {
                case Segment.Slope.Diagonal actual:
                    Assert.AreEqual(expectedSlope, actual.Item, Constants.tolerance);
                    break;
                default:
                    Assert.Fail("Unable to unpack slope.");
                    break;
            }
        }

        [Test]
        public void Slope_DeclineSegment_NegativeSlope()
        {
            var seg =
                Segment.create(
                    Point.create(2.0, 12.0),
                    Point.create(10.0, 2.0));
            var expectedSlope = -(10.0 / 8.0);

            var actualSlope = Segment.slope(seg);

            Assert.IsTrue(actualSlope.IsDiagonal, "Unexpected slope type returned.");
            switch (actualSlope)
            {
                case Segment.Slope.Diagonal actual:
                    Assert.AreEqual(expectedSlope, actual.Item, Constants.tolerance);
                    break;
                default:
                    Assert.Fail("Unable to unpack slope.");
                    break;
            }
        }

        [Test]
        public void Slope_HorizontalSegment_HorizontalSlope()
        {
            var seg =
                Segment.create(
                    Point.create(3.0, 7.0),
                    Point.create(9.0, 7.0));
            var expectedSlope = 7.0;

            var actualSlope = Segment.slope(seg);

            Assert.IsTrue(actualSlope.IsHorizontal, "Unexpected slope type returned.");
            switch (actualSlope)
            {
                case Segment.Slope.Horizontal actual:
                    Assert.AreEqual(expectedSlope, actual.Item, Constants.tolerance);
                    break;
                default:
                    Assert.Fail("Unable to unpack slope.");
                    break;
            }
        }

        [Test]
        public void Slope_VerticalSegment_VerticalSlope()
        {
            var seg =
                Segment.create(
                    Point.create(5.0, 4.0),
                    Point.create(5.0, 10.0));
            var expectedSlope = 5.0;

            var actualSlope = Segment.slope(seg);

            Assert.IsTrue(actualSlope.IsVertical, "Unexpected slope type returned.");
            switch (actualSlope)
            {
                case Segment.Slope.Vertical actual:
                    Assert.AreEqual(expectedSlope, actual.Item, Constants.tolerance);
                    break;
                default:
                    Assert.Fail("Unable to unpack slope.");
                    break;
            }
        }
    }
}
