﻿module EulerEquations.Geometry.Point

open EulerEquations.Core

type T = T of float*float

(*
* Creates a new Point.
*)
let create (x :float) (y :float) :T =
    (T (x, y))

(*
* Determines if two points are equal in value.
*)
let areEqual (T (x1, y1)) (T (x2, y2)) = 
    (Float.equals tolerance x1 x2) && (Float.equals tolerance y1 y2)