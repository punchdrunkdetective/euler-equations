﻿module EulerEquations.Geometry.Polygon

open EulerEquations.Core

type T = T of Point.T list

//TODO: solve is closed polygon problem (would aslo add more validation to the polygon?)

let private getDomainBounds polygon = 
    polygon 
    |> List.map (fun (Point.T (x, _)) -> x) 
    |> fun xs -> List.min xs, List.max xs

let private createSegments polygon = 
    
    let points = (List.head polygon)::(List.rev polygon) |> List.rev // Prepare list to connect last point to first point.

    let rec connect points segs  =
        match points with
        | [] -> List.rev segs 
        | [_] -> connect points segs
        | p1 :: p2 :: [] ->
            let s  = Segment.create p1 p2
            let segs' = s::segs
            connect [] segs'  
        | p1 :: p2 :: ps -> 
            let s = Segment.create p1 p2
            let segs' = s::segs
            let points' = p2::ps 
            connect points' segs' 

    connect points [] 

let private isEndPoint (Segment.T (p1, p2)) point =
    (Point.areEqual p1 point) || (Point.areEqual p2 point)

let private notEmpty points =
    match List.isEmpty points with 
    | true -> Error "cannot create polygon with no points"
    | false -> Ok points

let private hasMinPoints points =
    match List.length points >= 3 with 
    | false -> Error "Polygon composed of only one point."
    | true -> Ok points

let private isDistinct points =
    let distinctSize = List.length <| List.distinct points
    match List.length points = distinctSize with 
    | false -> Error "cannot create polygon with non distinct points"
    | true -> Ok points

(*
* Creates a new Polygon.
*)
let create (points :Point.T list) :T = 
    
    let isValidInput =
        Ok points
        |> Result.bind notEmpty
        |> Result.bind hasMinPoints
        |> Result.bind isDistinct

    match isValidInput with
    | Error e -> invalidArg "" e
    | Ok _ ->       
        (T points)

(*
* Returns true if the given point is inside the given polygon, false otherwise. 
* 
* O(n), where n = # of segments formed by the polygon
*)
let isPointInPolygon (p :Point.T) (T ps)  :bool =
    // Uses Ray-Casting algorithm to determine if a Point is in a Polygon.
    // Three edge cases to keep in mind.
    //      1. If the ray passes through a vertex, it passes through two segments. Count it only once. 
    //      2. If the ray passes through a segment parallel, it doesn't intersect. Don't count it.
    //      3. If the point is on the perimeter of the polygon, its in the polygon. Stop processing.

    let segs = createSegments ps
    let isPointOnSegment = Segment.onSegment p
    let onPerimeter = segs |> List.exists isPointOnSegment

    match onPerimeter with
    | true -> true
    | false -> 

        let (Point.T (x, y)) = p
        let lowerBound, upperBound = getDomainBounds ps
        let distanceTo bound = abs <| (x - bound)

        let rayDest = 
            match (distanceTo lowerBound) <= (distanceTo upperBound) with 
            | true -> lowerBound - 10.0 // You want to garrantee a ray cast to a point ouside the poly.
            | false -> upperBound + 10.0

        let ray = Segment.create (Point.create rayDest y) p

        let intersectsRay seg = 
            match Segment.intersection seg ray with //TODO: if you swap the params, this get a percision error.
            | Segment.Intersection.Cross intr -> 

                let (Point.T (x, y)) = intr

                let intersectionPoint = (Point.T (x, y))

                match intersectionPoint |> isEndPoint seg with
                | true -> // Only consider non parallel intersections, where one non endpoint is above the ray. (the Why gets mathy...)
                    let (Segment.T (p1, p2)) = seg
                    let (Point.T (_, y')) = [p1; p2] |> List.find (not << Point.areEqual intersectionPoint)
                    y' > y
                | _ -> true

            | _ -> false

        let rayIntersectionCount = 
            segs
            |> List.filter intersectsRay
            |> List.length 

        Int64.isOdd (int64 rayIntersectionCount)      

