﻿module EulerEquations.Geometry.Segment

open EulerEquations.Core

type T = T of Point.T * Point.T

type Slope =
    | Diagonal of float
    | Horizontal of float
    | Vertical of float

type Intersection =
    | Cross of Point.T 
    | Parrallel
    | None 

let private yInetercept m (Point.T (x, y)) =
    match m with
    | Vertical _-> Option.None
    | Horizontal m -> Some (Point.create 0.0 m)
    | Diagonal m -> 
        let intercept = y - (m * x) // from algebra: y = mx + b
        Some (Point.create 0.0 intercept)

let private inDomain x (T (p1, p2))  = 

    let (Point.T (x1, _)) = p1
    let (Point.T (x2, _)) = p2

    let segDomain = [x1; x2]
    let xLow = segDomain |> List.min
    let xHigh = segDomain |> List.max

    xLow <= x && x <= xHigh

let private inRange y (T (p1, p2))  = 

    let (Point.T (_, y1)) = p1
    let (Point.T (_, y2)) = p2

    let segRange = [y1; y2]
    let yLow = segRange |> List.min
    let yHigh = segRange |> List.max

    yLow <= y && y <= yHigh

let private pointsNotEqual (p1, p2) =
    match Point.areEqual p1 p2 with
    | true -> Error "Segment cannot have equal points"
    | false -> Ok (p1, p2)

(*
* Creates a new Segment.
*)
let create (p1 :Point.T) (p2 :Point.T) :T =
    
    let validInput =
        Ok (p1, p2) 
        |> Result.bind pointsNotEqual

    match validInput with 
    | Error e -> invalidArg "" e
    | Ok (p1, p2) -> (T (p1, p2))

(*
* Determines the slope of a segment.
*)
let slope (T (p1, p2)) :Slope =

    let (Point.T (x1, y1)) = p1
    let (Point.T (x2, y2)) = p2

    let yDelta = y2 - y1
    let xDelta = x2 - x1

    //NOTE: p1 = p2 would imply no slope, but we assume constraints enforced by create are held.
    match xDelta with
    | 0.0 -> Vertical x1 // x here is arbitrary
    | _ -> 
        match yDelta with 
        | 0.0 -> Horizontal y1 // y here is arbitrary
        | _ -> Diagonal ((y2 - y1) / (x2 - x1))

(*
* Determines the intersection of two segments.
*)
let intersection (seg1 :T) (seg2 :T) :Intersection =

    let m1 = slope seg1
    let m2 = slope seg2

    let (T (p1, _)) = seg1
    let (T (p2, _)) = seg2

    let b1 = yInetercept m1 p1
    let b2 = yInetercept m2 p2

    let solveForY (m :float) (b :float) (x :float) = (m * x) + b
    let solveForX (m :float) (b :float) (y :float) = (y - b) / m

    let output = 
        match (m1, m2) with

        | Vertical _, Vertical _ -> Parrallel
        | Horizontal _, Horizontal _ -> Parrallel

        | Vertical m1, Horizontal m2 -> Cross (Point.create m1 m2)
        | Horizontal m1, Vertical m2 -> Cross (Point.create m2 m1)

        | Diagonal m1, Vertical m2 -> 
            match b1 with
            | Option.None -> None
            | Some (Point.T (_, b1)) -> Cross (Point.create m2 (solveForY m1 b1 m2))
            
        | Diagonal m1, Horizontal m2 ->
            match b1 with 
            | Option.None -> None
            | Some (Point.T (_, b1)) -> Cross (Point.create (solveForX m1 b1 m2) m2)

        | Vertical m1, Diagonal m2 -> 
            match b2 with 
            | Option.None -> None
            | Some (Point.T (_, b2)) -> Cross (Point.create m1 (solveForY m2 b2 m1))
        | Horizontal m1, Diagonal m2 -> 
            match b2 with 
            | Option.None -> None
            | Some (Point.T (_, b2)) -> Cross (Point.create (solveForX m2 b2 m1) m1)

        | Diagonal m1, Diagonal m2 -> 

            match (Float.equals tolerance m1 m2), b1, b2 with 
            | true, _, _ -> Parrallel
            | _, Some (Point.T (_, b1)), Some (Point.T (_, b2)) -> 
                
                    let x = (b2 - b1) / (m1 - m2)
                    let y = (m1 * x) + b1
                    Cross (Point.create x y)

            | _ -> None

    match output with
    | Cross intr -> 
    
        let (Point.T (x, y)) = intr

        let segs = [seg1; seg2];
        let combine = List.fold (&&) true

        let inBothDomains = segs |> List.map (inDomain x) |> combine
        let inBothRanges = segs |> List.map (inRange y) |> combine

        match inBothDomains, inBothRanges with 
        | true, true -> output
        | _ -> None

    | _ -> output

(*
* Determines if a given point lies on the given segment.
*)
let onSegment (Point.T (x, y)) (seg :T)  :bool = 

    let m = slope seg

    let onSlope = 

        match m with 
        | Vertical vm -> Float.equals tolerance vm x
        | Horizontal hm -> Float.equals tolerance hm  y
        | Diagonal dm -> 

            let (T (p, _)) = seg // p here is arbitrary

            match yInetercept m p with 
            | Option.None -> false
            | Some (Point.T (_, b)) -> y |> Float.equals tolerance ((dm * x) + b)

    seg
    |> List.fold
        (fun f1 f2 seg -> f1 seg && f2 seg) 
        (fun _ -> true)
        [(fun _ -> onSlope);
         (inDomain x);
         (inRange y)]

