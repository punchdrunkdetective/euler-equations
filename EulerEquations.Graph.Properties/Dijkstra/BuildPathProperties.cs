﻿using System;
using System.Linq;
using Microsoft.FSharp.Collections;
using FsCheck;
using FsCheck.NUnit;

using TestFixture = NUnit.Framework.TestFixtureAttribute;

namespace EulerEquations.Graph.Properties
{
    [TestFixture]
    public partial class DijkstraProperties
    {
        public Arbitrary<Tuple<FSharpList<int>, int>> ArbBuildPathInput
        {
            get {
                var testPrev = ListModule.OfSeq(new int[] { -1, 0, 1, 2, 1, 3, });
                return Arb.From(Gen.zip(Gen.Constant(testPrev), Gen.Choose(0, testPrev.Length - 1)));
            }
        }

        [Property]
        public Property BuildPath_PathIsNonCyclic()
        {
            return Prop.ForAll(ArbBuildPathInput,
            input =>
            {
                Extract(input, out var prev, out var d);
                var path = Dijkstra.buildPath(prev, d);
                return path.Distinct().Count() == path.Count();
            });
        }

        [Property]
        public Property BuildPath_PathLengthLessThanNodeTotal()
        {
            return Prop.ForAll(ArbBuildPathInput,
            input =>
            {
                Extract(input, out var prev, out var d);
                var path = Dijkstra.buildPath(prev, d);
                return path.Count() <= prev.Count();
            });
        }

        [Property]
        public Property BuildPath_PathHasABeginning()
        {
            return Prop.ForAll(ArbBuildPathInput,
            input =>
            {
                Extract(input, out var prev, out var d);
                var path = Dijkstra.buildPath(prev, d);
                return path.IsEmpty == false ? prev[path.First()] == -1 : true;
            });
        }
    }
}
