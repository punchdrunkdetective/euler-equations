﻿using System;
using System.Linq;
using Microsoft.FSharp.Collections;
using FsCheck;
using FsCheck.NUnit;

using TestFixture = NUnit.Framework.TestFixtureAttribute;

namespace EulerEquations.Graph.Properties
{
    [TestFixture]
    public partial class DijkstraProperties
    {
        public const int TestNodeCount = 10;

        public Gen<Tuple<int, double[,]>> ShortestPathInputGen(int nodeCount) =>
            Gen.zip(NodeGen(nodeCount), GraphGen(nodeCount));

        public Arbitrary<Tuple<int, double[,]>> ArbShortestPathInput =>
             Arb.From(
                 ShortestPathInputGen(TestNodeCount),
                 input =>
                 {
                     Extract(input, out var s, out var m);

                     var nodeCount = Array2DModule.Length1(m) - 1;
                     return nodeCount > 0
                        ? Gen.Sample(10, 10, ShortestPathInputGen(nodeCount))
                        : FSharpList<Tuple<int, double[,]>>.Empty;
                 }); 

        [Property(EndSize = 1000)]
        public Property ShortestPath_AllPathsStartAtOrigin()
        {
            return Prop.ForAll(ArbShortestPathInput,
            input =>
            {
                Extract(input, out var s, out var m);

                var result = Dijkstra.shortestPath(m, s);
                Extract(result, out var prev, out var dist);

                var nodes = Enumerable.Range(0, prev.Length - 1);
                var startsAtOrigin = 
                    nodes
                    .Where(v => prev[v] != -1) // Remove start vertex and all unreacheable vetices from test.
                    .All(v =>
                    {
                        return s == GetStartOfPath(prev, v);
                    });

                return startsAtOrigin == true;
            });
        }

        [Property(EndSize = 1000)]
        public Property ShortestPath_AllDistancesSumUpFromOrigin()
        {
            return Prop.ForAll(ArbShortestPathInput,
            input =>
            {
                Extract(input, out var s, out var m);

                var result = Dijkstra.shortestPath(m, s);
                Extract(result, out var prev, out var dist);

                var nodes = Enumerable.Range(0, dist.Length - 1);
                var sumsFromOrigin =
                    nodes
                    .Where(v => prev[v] != -1) // Remove start vertex and all unreacheable vetices from test.
                    .All(v =>
                    {
                        var priorNode = prev[v];
                        return dist[v] == dist[priorNode] + m[priorNode, v];
                    });

                return sumsFromOrigin == true;
            });
        }

        [Property(EndSize = 1000)]
        public Property ShortestPath_OriginHasNoPreviousNode()
        {
            return Prop.ForAll(ArbShortestPathInput, 
            input =>
            {
                Extract(input, out var s, out var m);

                var result = Dijkstra.shortestPath(m, s);
                Extract(result, out var prev, out var dist);

                return prev[s] == -1;
            });
        }

        [Property(EndSize = 1000)]
        public Property ShortestPath_OriginHasNoDistance()
        {
            return Prop.ForAll(ArbShortestPathInput,
            input =>
            {
                Extract(input, out var s, out var m);

                var result = Dijkstra.shortestPath(m, s);
                Extract(result, out var prev, out var dist);

                return dist[s] == 0;
            });
        }

        [Property(EndSize = 1000)]
        public Property ShortestPath_DisconnectedNodesAreUnReachable()
        {
            return Prop.ForAll(ArbShortestPathInput,
            input =>
            {
                Extract(input, out var s, out var m);

                var result = Dijkstra.shortestPath(m, s);
                Extract(result, out var prev, out var dist);

                var nodes = Enumerable.Range(0, dist.Length - 1);
                var reachable = 
                    nodes
                    .Where(v => v != s && prev[v] == -1) // Get all unreachable nodes.
                    .Any(v => double.IsInfinity(dist[v]) == false); // Double check they are not reachable.

                return reachable == false;
            });
        }
    }
}
