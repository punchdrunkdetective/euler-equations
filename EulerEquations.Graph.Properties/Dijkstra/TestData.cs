﻿using System;
using System.Linq;
using Microsoft.FSharp.Collections;
using FsCheck;
using FsCheck.NUnit;

using TestFixture = NUnit.Framework.TestFixtureAttribute;

namespace EulerEquations.Graph.Properties
{
    [TestFixture]
    public partial class DijkstraProperties
    {
        public Gen<double> DoubleGen => // Positive non-zero values.
            Arb.Generate<double>()
                .Select(d => double.IsInfinity(d) ? d : Math.Round(d, 3))
                .Where(d => !double.IsNaN(d) && d != double.MaxValue && d > 0);

        public Gen<int> NodeGen(int nodeCount) =>
            Gen.Choose(0, nodeCount - 1);

        public Gen<double[,]> GraphGen(int nodeCount) =>
            Gen.Array2DOf(nodeCount, nodeCount, DoubleGen).Select(RemoveSelfReferences);

        private double[,] RemoveSelfReferences(double[,] graph)
        {
            var dim1Size = Array2DModule.Length1(graph);
            var dim2Size = Array2DModule.Length2(graph);

            var newGraph = new double[dim1Size, dim2Size];
            for (var i = 0; i < dim1Size; i++)
            {
                for (var j = 0; j < dim2Size; j++)
                {
                    newGraph[i, j] = i == j ? 0 : graph[i, j];
                }
            }
            return newGraph;
        }

        private int GetStartOfPath(FSharpList<int> prev, int dest)
        {
            return GetStartOfPath(prev.ToArray(), dest);
        }

        private int GetStartOfPath(int[] prev, int dest)
        {
            var toNode = dest;
            var fromNode = prev[toNode];

            var iterCount = 0; // prev could hold cyclic paths if iteration count > prev length
            while (fromNode >= 0 && iterCount < prev.Length - 1)
            {
                toNode = fromNode;
                fromNode = prev[toNode];

                iterCount++;
            }

            return toNode;
        }

        private void Extract<T1, T2>(Tuple<T1, T2> t, out T1 item1, out T2 item2)
        {
            item1 = t.Item1;
            item2 = t.Item2;
        }
    }
}
