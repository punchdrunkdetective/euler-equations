﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

using Microsoft.FSharp.Collections;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class AdjacencyMatrixTests
    {
        [Test]
        public void ChildrensOf_NegativeVertex_Exception()
        {
            var inputVertex = -1;
            var inputMatrix = new bool[,]
            {
                { false, false, true, true, },
                { true, false, false, false, },
                { false, true, false, false, },
                { false, true, false, false, },
            };

            Assert.Throws<ArgumentException>(() => AdjacencyMatrix.children(inputMatrix, inputVertex));
        }

        [Test]
        public void ChildrenOf_VertexBeyondBounds_Exception()
        {
            var inputVertex = 4;
            var inputMatrix = new bool[,]
            {
                { false, false, true, true, },
                { true, false, false, false, },
                { false, true, false, false, },
                { false, true, false, false, },
            };

            Assert.Throws<ArgumentException>(() => AdjacencyMatrix.children(inputMatrix, inputVertex));
        }

        [Test]
        public void ChildrenOf_VertexWithParents_ListOfParents()
        {
            var inputVertex = 0;
            var inputMatrix = new bool[,]
            {
                { false, false, true, true, },
                { true, false, false, false, },
                { false, true, false, false, },
                { false, true, false, false, },
            };
            var expectedOutput = ListModule.OfSeq(new List<int> { 2, 3 });

            var actualOutput = AdjacencyMatrix.children(inputMatrix, inputVertex);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void ChildrenOf_VertexWithParents_ListOfSizeParentCount()
        {
            var inputVertex = 0;
            var inputMatrix = new bool[,]
            {
                { false, false, true, true, },
                { true, false, false, false, },
                { false, true, false, false, },
                { false, true, false, false, },
            };
            var expectedOutput = ListModule.OfSeq(new List<int> { 2, 3, });

            var actualOutput = AdjacencyMatrix.children(inputMatrix, inputVertex);

            Assert.AreEqual(expectedOutput.Length, actualOutput.Length);
        }
    }
}
