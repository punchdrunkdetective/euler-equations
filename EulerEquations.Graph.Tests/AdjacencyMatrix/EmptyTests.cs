﻿using System;
using NUnit.Framework;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class AdjacencyMatrixTests
    {
        [Test]
        public void Empty_NegativeVertices_Exception()
        {
            var input = -100;

            Assert.Throws<ArgumentException>(() => AdjacencyMatrix.empty(input));
        }

        [Test]
        public void Empty_ZeroVertices_Exception()
        {
            var input = 0;

            Assert.Throws<ArgumentException>(() => AdjacencyMatrix.empty(input));
        }

        [Test]
        public void Empty_PositiveVertices_EmptyGraph()
        {
            var input = 100;

            var output = AdjacencyMatrix.empty(input);

            foreach (var edge in output)
            {
                Assert.IsFalse(edge);
            }
        }

        [Test]
        public void Empty_PositiveVertices_nxnGraph()
        {
            var input = 100;

            var output = AdjacencyMatrix.empty(input);

            Assert.AreEqual(input, output.GetLength(0), "Unexpected width.");
            Assert.AreEqual(input, output.GetLength(1), "Unexpected height.");
        }
    }
}
