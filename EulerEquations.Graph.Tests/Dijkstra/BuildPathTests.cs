﻿using System;
using NUnit.Framework;

using Microsoft.FSharp.Collections;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class DijkstraTests
    {
        [Test]
        public void BuildPath_ReachableNode_Path()
        {
            var d = 5;
            var expectedPath = ListModule.OfSeq(new int[] {0, 1, 2, 3, 5});

            var actualPath = Dijkstra.buildPath(_connectedPrev, d);
            
            TestUtilities.AssertFSharpListsAreEqual(expectedPath, actualPath);
        }

        [Test]
        public void BuildPath_NonReachableNode_NoPath()
        {
            var d = 8;

            var actualPath = Dijkstra.buildPath(_connectedPrev, d);

            Assert.IsEmpty(actualPath);
        }

        [Test]
        public void BuildPath_StartEqualsDestination_NoPath()
        {
            var d = 0;

            var actualPath = Dijkstra.buildPath(_connectedPrev, d);

            Assert.IsEmpty(actualPath);
        }

        [Test]
        public void BuildPath_DisconnectedGraph_NoPath()
        {
            var d = 2;

            var actualPath = Dijkstra.buildPath(_disconnectedPrev, d);

            Assert.IsEmpty(actualPath);
        }

        [Test]
        public void BuildPath_EmptyPrev_ArguementException()
        {
            var d = 5;
            var prev = ListModule.OfSeq(new int[] { });
            Assert.Throws<ArgumentException>(() => Dijkstra.buildPath(prev, d));
        }

        [Test]
        public void BuildPath_NullPrev_ArguementException()
        {
            var d = 5;
            FSharpList<int> prev = null;
            Assert.Throws<ArgumentException>(() => Dijkstra.buildPath(prev, d));
        }

        [TestCase(-1)]
        [TestCase(10)]
        public void BuildPath_NonExistantDestination_ArguementException(int d)
        {
            Assert.Throws<ArgumentException>(() => Dijkstra.buildPath(_connectedPrev, d));
        }

        [Test]
        public void BuildPath_CyclicPrev_ArguementException()
        {
            var d = 5;
            var prev = ListModule.OfSeq(new int[] { 2, 0, 1, 2, 1, 3, -1, -1, -1, -1 });

            Assert.Throws<ArgumentException>(() => Dijkstra.buildPath(prev, d));
        }
    }
}
