﻿using System;
using NUnit.Framework;

using Microsoft.FSharp.Collections;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class DijkstraTests
    {
        [Test]
        public void ShortestPath_ConnectedGraph_Paths()
        {
            var output = Dijkstra.shortestPath(_connectedGraph, _connectedStart);
            var actualPrev = output.Item1;

            TestUtilities.AssertFSharpListsAreEqual(_connectedPrev, actualPrev);
        }
        
        [Test]
        public void ShortestPath_ConnectedGraph_Distances()
        {
            var output = Dijkstra.shortestPath(_connectedGraph, _connectedStart);
            var actualDist = output.Item2;

            TestUtilities.AssertFSharpListsAreEqual(_connectedDist, actualDist);
        }

        [Test]
        public void ShortestPath_DisconnectedGraph_NoPaths()
        {
            var output = Dijkstra.shortestPath(_disconnectedGraph, 0);
            var actualPrev = output.Item1;

            TestUtilities.AssertFSharpListsAreEqual(_disconnectedPrev, actualPrev);
        }

        [Test]
        public void ShortestPath_DisconnectedGraph_NoDistances()
        {
            var output = Dijkstra.shortestPath(_disconnectedGraph, 0);
            var actualDist = output.Item2;

            TestUtilities.AssertFSharpListsAreEqual(_disconnectedDist, actualDist);
        }

        [Test]
        public void ShortestPath_EmptyGraph_ArguementException()
        {
            Assert.Throws<ArgumentException>(() => Dijkstra.shortestPath(new double[,] { }, _connectedStart));
        }

        [Test]
        public void ShortestPath_NegativeEdge_ArguementException()
        {
            double[,] testGraph = new double[,]
            {
                {  0,  3,  _i, _i, },
                {  3,  0, -15, 10, },
                { _i, 15,   0,  4, },
                { _i, 10,   4,  0, },
             };

            Assert.Throws<ArgumentException>(() => Dijkstra.shortestPath(testGraph, 0));
        }

        [Test]
        public void ShortestPath_ZeroEdge_ArguementException()
        {
            double[,] testGraph = new double[,]
            {
                {  0,  3, _i, _i, },
                {  3,  0,  0, 10, },
                { _i, 15,  0,  4, },
                { _i, 10,  4,  0, },
            };

            Assert.Throws<ArgumentException>(() => Dijkstra.shortestPath(testGraph, 0));
        }

        [Test]
        public void ShortestPath_SelfReferencingNode_ArguementException()
        {
            double[,] testGraph = new double[,]
            {
                {  0,  3, _i, _i, },
                {  3,  2, 15, 10, },
                { _i, 15,  0,  4, },
                { _i, 10,  4,  0, },
             };

            Assert.Throws<ArgumentException>(() => Dijkstra.shortestPath(testGraph, 0));
        }

        [Test]
        public void ShortestPath_NonSquareGraph_ArguementException()
        {
            double[,] testGraph = new double[,]
            {
                {  0,  3, _i, _i, },
                {  3,  0, 15, 10, },
                { _i, 15,  0,  4, },
             };

            Assert.Throws<ArgumentException>(() => Dijkstra.shortestPath(testGraph, 0));
        }

        [TestCase(-1, TestName = nameof(ShortestPath_NonExistantStart) + "_BelowRange_ArguementException")]
        [TestCase(10, TestName = nameof(ShortestPath_NonExistantStart) + "_AboverRange_ArguementException")]
        public void ShortestPath_NonExistantStart(int start)
        {
            Assert.Throws<ArgumentException>(() => Dijkstra.shortestPath(_connectedGraph, start));
        }
    }
}
