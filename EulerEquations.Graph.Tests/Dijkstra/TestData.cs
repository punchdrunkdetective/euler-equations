﻿using NUnit.Framework;
using Microsoft.FSharp.Collections;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class DijkstraTests
    {
        private const double _i = double.PositiveInfinity;

        /* Connected Graph:   
         *        1.    3.           6.    7.
         *         O-----O            O-----O
         *        /|\   /|\            \   /
         *    0. / | \ / | \ 5.         \ /
         *      O  |  /  |  O            O
         *       \ | / \ | /            8.
         *        \|/   \|/                          9.
         *         O-----O                            O
         *        2.    4.
         */
        private readonly double[,] _connectedGraph = new double[,]
        {
                {  0,   3,   30,   _i,   _i,  _i,  _i,  _i,  _i, _i, },
                {  3,   0,   13,   55,   12,  _i,  _i,  _i,  _i, _i, },
                { 30,  13,    0,    7,    5,  _i,  _i,  _i,  _i, _i, },
                {  _i,  55,   7,    0,  102,   2,  _i,  _i,  _i, _i, },
                {  _i,  12,   5,  102,    0,  83,  _i,  _i,  _i, _i, },
                {  _i,  _i,  _i,    2,   83,   0,  _i,  _i,  _i, _i, },
                {  _i,  _i,  _i,   _i,   _i,  _i,   0,  74,  92, _i, },
                {  _i,  _i,  _i,   _i,   _i,  _i,  74,   0,  35, _i, },
                {  _i,  _i,  _i,   _i,   _i,  _i,  92,  35,   0, _i, },
                {  _i,  _i,  _i,   _i,   _i,  _i,  _i,  _i,  _i,  0, },
        };
        private const int _connectedStart = 0;
        private readonly FSharpList<int> _connectedPrev = ListModule.OfSeq(new int[] { -1, 0, 1, 2, 1, 3, -1, -1, -1, -1 });
        private readonly FSharpList<double> _connectedDist = ListModule.OfSeq(new double[] { 0, 3, 16, 23, 15, 25, _i, _i, _i, _i });

        /*
         * Disconnected Graph: 
         *      0.      2.
         *       O       O
         *       
         *       
         *      1.      3.
         *       O       O
         */
        private double[,] _disconnectedGraph = new double[,]
        {
            {  0, _i, _i, _i, },
            { _i,  0, _i, _i, },
            { _i, _i,  0, _i, },
            { _i, _i, _i,  0, },
        };
        private FSharpList<int> _disconnectedPrev = ListModule.OfSeq(new int[] { -1, -1, -1, -1 });
        private FSharpList<double> _disconnectedDist = ListModule.OfSeq(new double[] { 0, _i, _i, _i });
    }
}
