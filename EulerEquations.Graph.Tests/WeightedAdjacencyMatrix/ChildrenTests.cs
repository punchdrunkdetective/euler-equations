﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class WeightedAdjacencyMatrixTests
    {
        [Test]
        public void Children_NegativeVertex_ArgumentException()
        {
            var inputVertex = -1;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1, },
                { 1, 0, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 1, 0, 0, },
            };

            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.children(inputMatrix, NonZeroEdge(inputMatrix), inputVertex));
        }

        [Test]
        public void Children_VertexBeyondBounds_ArgumentException()
        {
            var inputVertex = 4;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1, },
                { 1, 0, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 1, 0, 0, },
            };

            Assert.Throws<ArgumentException>(() =>  WeightedAdjacencyMatrix.children(inputMatrix, NonZeroEdge(inputMatrix), inputVertex));
        }

        [Test]
        public void Children_NonSquareMatrix_ArguementException()
        {
            var inputVertex = 0;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1, },
                { 1, 0, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 0, 0, 0, },
            };

            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.children(inputMatrix, NonZeroEdge(inputMatrix), inputVertex));
        }

        [Test]
        public void Children_VertexWithChildren_ListOfChildren()
        {
            var inputVertex = 0;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1, },
                { 1, 0, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 1, 0, 0, },
            };
            ;
            var expectedOutput = ListModule.OfSeq(new List<int> { 2, 3, });
            var actualOutput = WeightedAdjacencyMatrix.children(inputMatrix, NonZeroEdge(inputMatrix), inputVertex);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutput, actualOutput);
        }
    }
}
