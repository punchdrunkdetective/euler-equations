﻿using System;
using NUnit.Framework;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class WeightedAdjacencyMatrixTests
    {
        [Test]
        public void Empty_NegativeVertices_ArgumentException()
        {
            var size = -100;
            var defaultVal = -1;
            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.empty(size, defaultVal));
        }

        [Test]
        public void Empty_ZeroVertices_ArgumentException()
        {
            var size = 0;
            var defaultVal = -1;
            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.empty(size, defaultVal));
        }

        [Test]
        public void Empty_PositiveVertices_DefaultValueFilledGraph()
        {
            var size = 100;
            var defaultVal = -1;

            var output = WeightedAdjacencyMatrix.empty(size, defaultVal);

            foreach (var edge in output)
            {
                Assert.AreEqual(defaultVal, edge);
            }
        }

        [Test]
        public void Empty_PositiveVertices_nxnGraph()
        {
            var size = 100;
            var defaultVal = -1;

            var output = WeightedAdjacencyMatrix.empty(size, defaultVal);

            Assert.AreEqual(size, output.GetLength(0), "Unexpected width.");
            Assert.AreEqual(size, output.GetLength(1), "Unexpected height.");
        }
    }
}
