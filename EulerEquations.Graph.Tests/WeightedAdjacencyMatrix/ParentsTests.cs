﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class WeightedAdjacencyMatrixTests
    {
        [Test]
        public void Parents_NegativeVertex_ArgumentException()
        {
            var inputVertex = -1;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1},
                { 1, 0, 0, 0},
                { 0, 1, 0, 0},
                { 0, 1, 0, 0},
            };

            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.parents(inputMatrix, NonZeroEdge(inputMatrix), inputVertex));
        }

        [Test]
        public void Parents_VertexBeyondBounds_ArgumentException()
        {
            var inputVertex = 4;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1},
                { 1, 0, 0, 0},
                { 0, 1, 0, 0},
                { 0, 1, 0, 0},
            }; ;

            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.parents(inputMatrix, NonZeroEdge(inputMatrix), inputVertex));
        }

        [Test]
        public void Parents_NonSquareMatrix_ArguementException()
        {
            var inputVertex = 0;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1, },
                { 1, 0, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 1, 0, 0, },
                { 0, 0, 0, 0, },
            };

            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.parents(inputMatrix, NonZeroEdge(inputMatrix), inputVertex));
        }

        [Test]
        public void Parents_VertexWithParents_ListOfParents()
        {
            var inputVertex = 1;
            var inputMatrix = new int[,]
            {
                { 0, 0, 1, 1},
                { 1, 0, 0, 0},
                { 0, 1, 0, 0},
                { 0, 1, 0, 0},
            };

            var expectedOutput = ListModule.OfSeq(new List<int> { 2, 3, });
            var actualOutput = WeightedAdjacencyMatrix.parents(inputMatrix, NonZeroEdge(inputMatrix), inputVertex);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutput, actualOutput);
        }
    }
}
