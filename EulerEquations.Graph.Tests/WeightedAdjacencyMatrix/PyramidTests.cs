﻿using System;
using NUnit.Framework;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class WeightedAdjacencyMatrixTests
    {
        [Test]
        public void Pyramid_NegativeVertices_ArgumentException()
        {
            var input = -100;
            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.pyramid(input));
        }

        [Test]
        public void Pyramid_ZeroVertices_ArgumentExceptionn()
        {
            var input = 0;
            Assert.Throws<ArgumentException>(() => WeightedAdjacencyMatrix.pyramid(input));
        }

        [Test]
        public void Pyramid_PositiveVertices_nxnGraph()
        {
            var input = 100;

            var output = WeightedAdjacencyMatrix.pyramid(input);

            Assert.AreEqual(input, output.GetLength(0), "Unexpected width.");
            Assert.AreEqual(input, output.GetLength(1), "Unexpected height.");
        }

        [Test]
        public void Pyramid_OneVertex_OneTierPyramid()
        {
            var input = 1;
            var expectedOutput = new int[,]
            {
                { 0 },
            };

            var actualOutput = WeightedAdjacencyMatrix.pyramid(input);

            AssertMatricesAreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Pyramid_ThreeVertices_TwoTierPyramid()
        {
            int input = 3;
            int[,] expectedOutput = new int[,]
            {
                { 0, 1, 1 },
                { 0, 0, 0 },
            };

            int[,] actualOutput = WeightedAdjacencyMatrix.pyramid(input);

            AssertMatricesAreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Pyramid_SixVertices_ThreeTierPyramid()
        {
            int input = 6;
            int[,] expectedOutput = new int[,]
            {
                { 0, 1, 1, 0, 0, 0 },
                { 0, 0, 0, 1, 1, 0 },
                { 0, 0, 0, 0, 1, 1 },
                { 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0 },
            };

            int[,] actualOutput = WeightedAdjacencyMatrix.pyramid(input);

            AssertMatricesAreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Pyramid_TenVertices_FourTierPyramid()
        {
            int input = 10;
            int[,] expectedOutput = new int[,]
            {
                { 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            };

            int[,] actualOutput = WeightedAdjacencyMatrix.pyramid(input);

            AssertMatricesAreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Pyramid_ElevenVertices_FourTierIncompletePyramid()
        {
            /*
            *     1    
            *    1 1
            *   1 1 1   
            *  1 1 1 1  
            * 1                
            */

            int input = 11;
            int[,] expectedOutput = new int[,]
            {
                { 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            };

            int[,] actualOutput = WeightedAdjacencyMatrix.pyramid(input);

            AssertMatricesAreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void Pyramid_TwelveVertices_FourTierIncompletePyramid()
        {
            /*
            *     1    
            *    1 1
            *   1 1 1   
            *  1 1 1 1  
            * 1 1             
            */

            int input = 12;
            int[,] expectedOutput = new int[,]
            {
                { 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            };

            int[,] actualOutput = WeightedAdjacencyMatrix.pyramid(input);

            AssertMatricesAreEqual(expectedOutput, actualOutput);
        }
    }
}
