﻿using System;
using Microsoft.FSharp.Core;
using NUnit.Framework;

namespace EulerEquations.Graph.Tests
{
    [TestFixture]
    public partial class WeightedAdjacencyMatrixTests
    {
        private FSharpFunc<Tuple<int, int>, bool> NonZeroEdge(int[,] matrix)
        {
            return FuncConvert.ToFSharpFunc<Tuple<int, int>, bool>(
                edge =>
                {
                    var u = edge.Item1;
                    var v = edge.Item2;
                    return matrix[u, v] != 0;
                }
            );
        }

        private void AssertMatricesAreEqual(int[,] expected, int[,] actual)
        {
            for (int i = 0; i < expected.GetLength(0); i++)
            {
                for (int j = 0; j < expected.GetLength(1); j++)
                {
                    Assert.AreEqual(expected[i, j], actual[i, j], $"element: {i}, {j}");
                }
            }
        }
    }
}
