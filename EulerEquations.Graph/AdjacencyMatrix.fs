﻿module EulerEquations.Graph.AdjacencyMatrix

let private edgeExists (m :bool[,]) (u, v) = m.[u, v] 

(* 
* Creates an empty adjacency matrix for a graph of n nodes. 
*)
let empty (n :int32) :bool[,] =  
    WeightedAdjacencyMatrix.empty n false

(* 
* Returns a list of all v's parent nodes in the given adjacency matrix. 
*)
let parents (m :bool[,]) (v :int32) :int32 list =  
    WeightedAdjacencyMatrix.parents m (edgeExists m) v

(* 
* Returns a list of all u's children nodes in the given adjacency matrix. 
*)
let children (m :bool[,]) (u :int32) :int32 list = 
    WeightedAdjacencyMatrix.children m (edgeExists m) u

