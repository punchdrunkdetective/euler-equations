﻿module EulerEquations.Graph.Dijkstra

open System
open DijkstraValidation
open EulerEquations.Core

(*
* An implimentation of Dijktra's algorithm to find paths from s to all v in m, along with those path's distances.
* The path's are outputed in a prev list, such that prev[v] = u, i.e. u is the previous vertex to v in the shortest 
* from s. The distances are outputed in a dist list such that dist[v] = min distance from s to v. 
* 
* The following properties should be true for m:
*   - Vertex's should not be self referencing.
*   - All edge's should have positive, non-zero weights.
*   - Should have correct deminsions for an adjanency matrix. (i.e. square)
*
* O(V^2) where V is the set of all vertices.
*)
let shortestPath (m :float[,]) (s :int32) :(int32 list * float list) = 

    let validInput = //NOTE: Original implimentation does not consider double edges (check wiki).       
        Ok (m, s) 
        |> Result.bind isVertex
        |> Result.map (fun _ -> m)
        |> Result.bind isSquareMatrix
        |> Result.bind hasPositiveNonZeroEdges
        |> Result.bind hasNonSelfReferencingVertices
        |> Result.map (fun _ -> m, s)

    match validInput with 
    | Error e -> invalidArg "" e
    | Ok (m, s) -> 
        
        let vertexCount = Array2D.length1 m
        let vertices = List.init vertexCount (fun i -> i)

        let initPrev = List.init vertexCount (fun _ -> -1)
        let initDist = List.init vertexCount (fun i -> match i = s with true -> 0.0 | _ -> Double.PositiveInfinity)
        let initUnvisited = List.zip vertices initDist 
        
        let rec visitNode prev dist unvisited =

            match List.isEmpty unvisited with
            | true -> prev, dist
            | false -> 

                let u, uDist = unvisited |> List.minBy (fun (_, d) -> d)
                let adjDist v = uDist + m.[u, v]

                let dist' = //NOTE: Original implimentation only considers unvisited nodes (check wiki).
                    dist 
                    |> List.mapi (fun v vDist -> Float.min tolerance vDist (adjDist v))
                    
                let prev' = 
                    prev 
                    |> List.map3 (fun d d' p -> 
                        match d <= d' with 
                        | true -> p
                        | false -> u) dist dist'

                let unvisited' = 
                    unvisited 
                    |> List.filter (fun (v, _) -> v <> u) // Mark current node as visited.
                    |> List.map (fun (v, vDist) -> v, Float.min tolerance vDist (adjDist v))

                visitNode prev' dist' unvisited'

        visitNode initPrev initDist initUnvisited
 
(*
* Given a prev list such that prev[v] = previous vertex u in a path from a s to v, builds a path from s to v.
* 
* O(V^2) where V is the set of all vertices.
*)
let buildPath (prev :int list) (d :int) :int list =

    let validInput =
        Ok (prev, d) 
        |> Result.map (fun _ -> prev)
        |> Result.bind isNotNull //TODO: you probably should throw a null arguement exception.
        |> Result.bind isNotEmpty
        |> Result.map (fun _ -> prev, d)
        |> Result.bind isIndex
        |> Result.map (fun _ -> prev, d)

    match validInput with
    | Error e -> invalidArg "" e
    | Ok (prev, d) ->            
        match (prev |> List.item d) = -1 with 
        | true -> List.empty // Destination is either the source or is unreachable.
        | false ->

            let prevSize = prev |> List.length

            let rec getPath prev i path = //TODO: surely there is a way to make this linear :(
                let p = prev |> List.item i
                let pathSize = List.length path // Guard against cyclic paths.
                match p = -1 || pathSize >= prevSize with 
                | true -> i::path
                | false -> getPath prev p (i::path)
            let path = getPath prev d []

            match List.length path >= prevSize with
            | true -> invalidArg "prev" "prev list contains cyclic path."
            | false -> path
