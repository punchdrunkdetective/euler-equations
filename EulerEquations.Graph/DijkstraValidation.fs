﻿namespace EulerEquations.Graph

(*
* Provides input validators to Dijkstra Algorithms.  
*)
module internal DijkstraValidation = 
    open EulerEquations.Core

    let private filterEdges m cond =
        //TODO: Move this function to a matrix module. Seems handy.
        let vertices = [0..(Array2D.length1 m) - 1]
        let children cond = WeightedAdjacencyMatrix.children m cond
        vertices |> List.collect 
            (fun u -> 
                u
                |> children cond 
                |> List.map (fun v -> u, v))

    let private edgeExists m cond = 
        //TODO: Move this function to a matrix module. Seems handy.
        let edges = filterEdges m cond
        not <| List.isEmpty edges

    (*
    * Validates that vertex v exists in matrix m.
    *)
    let isVertex (m, v) =
        match 0 <= v && v < Array2D.length1 m with
        | true -> Ok (m, v)
        | false -> Error (sprintf "invalid vertex given: %i" v)

    (*
    * Validates that index i exists in list l.
    *)
    let isIndex (l, i) =
        match 0 <= i && i < List.length l with
        | true -> Ok (l, i)
        | false -> Error (sprintf "invalid index given: %i for list" i)

    (*
    * Validates that the given matrix is square.
    *)
    let isSquareMatrix m =
        let dim1 = Array2D.length1 m
        let dim2 = Array2D.length2 m
        match dim1 = dim2 with 
        | true -> Ok m
        | false -> Error (sprintf "non square mattrix: %i x %i" dim1 dim2)

    (*
    * Validates that the given matrix has positive, non zero edges.
    *)
    let hasPositiveNonZeroEdges m =        
        let found = edgeExists m (fun (u, v) -> (u <> v) && (Float.equals tolerance m.[u, v] 0.0 || m.[u, v] < -0.0))
        match not found with 
        | true -> Ok m
        | false -> Error "negative or zero edge found"

    (*
    * Validates that the given matrix has no self referencing vertices.
    *)
    let hasNonSelfReferencingVertices m =     
        let found = edgeExists m (fun (u, v) -> (u = v) && (not <| Float.equals tolerance m.[u, v] 0.0))
        match not found with 
        | true -> Ok m
        | false -> Error "self referencing vertext found"

    (*
    * Validates that an object is not null.
    *)
    let isNotNull o =
        match isNull (box o) with
        | false -> Ok o
        | true -> Error "object cannot be null"

    (*
    * Validates that a list is not empty.
    *)
    let isNotEmpty l =
        match List.isEmpty l with 
        | false -> Ok l
        | true -> Error "list cannot be empty"
            

