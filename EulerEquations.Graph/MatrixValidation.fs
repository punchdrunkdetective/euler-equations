﻿namespace EulerEquations.Graph

(*
* Provides input validators to Matrix Modules.  
*)
module internal MatrixValidation =

    (*
    * Validates that x is a positive, non-zero value.
    *)
    let isPositiveNonZero x = 
        match x > 0 with
        | true -> Ok x 
        | false -> Error (sprintf "negative or zero value given: %i" x)

    (*
    * Validates that a vertex v exist in matrix m.
    *)
    let isVertex (m, v) =
        let size = Array2D.length1 m
        match 0 <= v && v < size with
        | true -> Ok (m, v)
        | false -> Error (sprintf "invalid vertex given: %i" v)

    (*
    * Validates that a matrix m is square, i.e. number of rows equals number of columns.
    *)
    let isSquare m =         
        let w = Array2D.length2 m
        let h = Array2D.length1 m
        match w = h with
        | true -> Ok m
        | false -> Error (sprintf "invalid matrix deminsions: %i x %i" w h)

    

