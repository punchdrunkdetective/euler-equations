﻿module EulerEquations.Graph.WeightedAdjacencyMatrix

open MatrixValidation

(* 
* Creates an empty weighted adjacency matrix for a graph of n nodes using the given default value for an edge. 
*)
let empty (n :int32) (defaultVal :'T) :'T[,] =  

    let validInput =
        Ok n
        |> Result.bind isPositiveNonZero
        |> Result.map (fun _ -> n)

    match validInput with
    | Error e -> invalidArg "" e
    | Ok n -> Array2D.create n n defaultVal
  
(* 
* Creates a weighted adjacency matrix for a graph of n nodes where the edges have a pyramid
* structure starting from vertex 0 directed down. All edges will have a default value of 1, and all non-edges will
* have a default value of 0. A simple map can change these defaults if other values are desired. 
* 
* Example: n = 6
*   1      [0 1 1 0 0 0 
*  1 1  ->  0 0 0 1 1 0
* 1 1 1     0 0 0 0 1 1
*           0 0 0 0 0 0
*           0 0 0 0 0 0
*           0 0 0 0 0 0]
*)
let pyramid (n :int32) :int32[,] = 

    let validInput =
        Ok n
        |> Result.bind isPositiveNonZero
        |> Result.map (fun _ -> n)

    match validInput with 
    | Error e -> invalidArg "" e
    | Ok n -> 

        let vertices = [0..(n - 1)]
        let isVertice v = 0 <= v && v < n       

        // Following relies on observation that any given tier of a pyramid contains at most tier id +1 vertices.
        // i.e. tier 0 has 1 vertex, tier 1 has 2 verices, etc.
        let rec createPryamid tier vertices pyramid =
            match vertices with 
            | [] -> List.rev pyramid
            | _ -> 

                let (tierVertices, vertices') =
                    match tier <= List.length vertices with // Is teir partial or filled.
                    | false -> vertices, []
                    | true -> List.splitAt (tier + 1) vertices      
                    
                createPryamid (tier + 1) vertices' (tierVertices :: pyramid)

        let pyramid = createPryamid 0 vertices []

        let tierToEdges tierVertices =        

            let tier = (List.length tierVertices) - 1   
            
            tierVertices 
            |> List.collect 
                (fun u -> 
                    let v1 = tier + u + 1
                    let v2 = tier + u + 2
                    [u,v1;u,v2]
                )
            |> List.filter (fun (_, v) -> isVertice v)

        let edges = pyramid |> List.collect tierToEdges

        let m = empty n 0
        edges |> List.iter (fun (u, v) -> m.[u, v] <- 1) 
        m    

(* 
* Finds all parents of vertex v that meet the given edge condition for (u, v). 
*
* O(V) where V is the set of vertices in matrix m.
*)
let parents (m :'T[,]) (cond :(int32 * int32) -> bool) (v :int32) :int32 list = 
 
    let validInput =
        Ok (m, v)
        |> Result.map (fun _ -> m)
        |> Result.bind isSquare
        |> Result.map (fun _ -> (m, v))
        |> Result.bind isVertex          
        |> Result.map (fun _ -> (m, v))

    match validInput with
    | Error e -> invalidArg "" e
    | Ok (m, v) -> 
        let totalVertexs = Array2D.length1 m
        let vertices = [0..(totalVertexs - 1)]
        vertices |> List.filter (fun u -> cond (u, v))

(* 
* Finds all children of vertex u that meet the given edge condition for (u, v). 
*
* O(V) where V is the set of vertices in matrix m.
*)
let children (m :'T[,]) (cond :(int32 * int32) -> bool) (u :int32) :int32 list = 
    
    let validInput =
        Ok (m, u)
        |> Result.map (fun _ -> m)
        |> Result.bind isSquare
        |> Result.map (fun _ -> (m, u))
        |> Result.bind isVertex           
        |> Result.map (fun _ -> (m, u))

    match validInput with
    | Error e -> invalidArg "" e
    | Ok (m, u) -> 
        let totalVertices = Array2D.length2 m
        let vertices = [0..(totalVertices - 1)]
        vertices |> List.filter (fun v -> cond (u, v))

