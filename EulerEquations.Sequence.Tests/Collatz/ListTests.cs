﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class CollatzTests
    {
        [Test]
        public void List_Negative_ArgumentException()
        {
            var input = -13L;
            Assert.Throws<ArgumentException>(() => Collatz.List(input));            
        }

        [Test]
        public void List_Zero_ArgumentException()
        {
            var input = 0L;
            Assert.Throws<ArgumentException>(() => Collatz.List(input));
        }

        [Test]
        public void List_Positive_CollatzSequence()
        {
            var input = 13L;
            var expectedOutput = ListModule.OfSeq(new List<long> { 13, 40, 20, 10, 5, 16, 8, 4, 2, 1 });

            var actualOutput = Collatz.List(input);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutput, actualOutput);
        }
    }
}
