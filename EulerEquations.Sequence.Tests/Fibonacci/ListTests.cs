﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class FibonacciTests
    {
        [Test]
        public void List_Negative_ArgumentException()
        {
            var n = -1;
            Assert.Throws<ArgumentException>(() => Fibonacci.list(n));
        }

        [Test]
        public void List_Zero_EmptyList()
        {
            var n = 0;

            var list = Fibonacci.list(n);

            Assert.IsTrue(list.IsEmpty);
        }

        [Test]
        public void List_One_FibList()
        {
            var n = 1;
            var expectedList = ListModule.OfSeq(new List<BigInteger> { 0, });

            var actualList = Fibonacci.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void List_Two_FibList()
        {
            var n = 2;
            var expectedList = ListModule.OfSeq(new List<BigInteger> { 0, 1, });

            var actualList = Fibonacci.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void List_Three_FibList()
        {
            var n = 3;
            var expectedList = ListModule.OfSeq(new List<BigInteger> { 0, 1, 1, });

            var actualList = Fibonacci.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }

        [Test]
        public void List_Ten_FibList()
        {
            var n = 10;
            var expectedList = ListModule.OfSeq(new List<BigInteger> { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, });

            var actualList = Fibonacci.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedList, actualList);
        }
    }
}
