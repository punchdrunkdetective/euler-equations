﻿using System;
using System.Numerics;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class FibonacciTests
    {
        [Test]
        public void nth_Negative_ArgumentException()
        {
            var n = -1;
            Assert.Throws<ArgumentException>(() => Fibonacci.nth(n));
        }

        [Test]
        public void nth_Zero_Zero()
        {
            var n = 0;
            var expectedFib = new BigInteger(0);

            var actualFib = Fibonacci.nth(n);

            Assert.AreEqual(expectedFib, actualFib);
        }

        [Test]
        public void nth_One_One()
        {
            var n = 1;
            var expectedFib = new BigInteger(1);

            var actualFib = Fibonacci.nth(n);

            Assert.AreEqual(expectedFib, actualFib);
        }

        [Test]
        public void nth_Two_One()
        {
            var n = 2;
            var expectedFib = new BigInteger(1);

            var actualFib = Fibonacci.nth(n);

            Assert.AreEqual(expectedFib, actualFib);
        }

        [Test]
        public void nth_Three_Two()
        {
            var n = 3;
            var expectedFib = new BigInteger(2);

            var actualFib = Fibonacci.nth(n);

            Assert.AreEqual(expectedFib, actualFib);
        }

        [Test]
        public void nth_Ten_FiftyFive()
        {
            var n = 10;
            var expectedFib = new BigInteger(55);

            var actualFib = Fibonacci.nth(n);

            Assert.AreEqual(expectedFib, actualFib);
        }
    }
}
