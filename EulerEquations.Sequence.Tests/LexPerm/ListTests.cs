﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Microsoft.FSharp.Collections;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class LexPermTests
    {
        [Test]
        public void LexPerm_NullList_ArgumentException()
        {
            FSharpList<int> inList = null;
            Assert.Throws<ArgumentException>(() => LexPerm.list(inList));
        }

        [Test]
        public void LexPerm_EmptyList_EmptyList()
        {
            var inList = ListModule.OfSeq(new List<int>());

            var outList = LexPerm.list(inList);

            Assert.IsTrue(outList.IsEmpty);
        }

        [Test]
        public void LexPerm_1_PermList()
        {
            var inList = ListModule.OfSeq(new List<int> { 1, });
            var expectedOutList = ListModule.OfSeq(new List<FSharpList<int>> { ListModule.OfSeq(new List<int> { 1, }), });

            var actualOutList = LexPerm.list(inList);

            Assert.AreEqual(expectedOutList.Length, actualOutList.Length, "lengths not equal");
            for (var i = 0; i < expectedOutList.Length; i++)
            {
                TestUtilities.AssertFSharpListsAreEqual(expectedOutList[i], actualOutList[i]);
            }
        }

        [Test]
        public void LexPerm_012_PermList()
        {
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<FSharpList<int>> {
                ListModule.OfSeq(new List<int> { 0, 1, 2, }),
                ListModule.OfSeq(new List<int> { 0, 2, 1, }),
                ListModule.OfSeq(new List<int> { 1, 0, 2, }),
                ListModule.OfSeq(new List<int> { 1, 2, 0, }),
                ListModule.OfSeq(new List<int> { 2, 0, 1, }),
                ListModule.OfSeq(new List<int> { 2, 1, 0, }),
            });

            var actualOutList = LexPerm.list(inList);

            Assert.AreEqual(expectedOutList.Length, actualOutList.Length, "lengths not equal");
            for (var i = 0; i < expectedOutList.Length; i++)
            {
                TestUtilities.AssertFSharpListsAreEqual(expectedOutList[i], actualOutList[i]);
            }
        }

        [Test]
        public void LexPerm_197_PermList()
        {
            var inList = ListModule.OfSeq(new List<int> { 1, 9, 7, });
            var expectedOutList = ListModule.OfSeq(new List<FSharpList<int>> {
                ListModule.OfSeq(new List<int> { 1, 7, 9, }),
                ListModule.OfSeq(new List<int> { 1, 9, 7, }),
                ListModule.OfSeq(new List<int> { 7, 1, 9, }),
                ListModule.OfSeq(new List<int> { 7, 9, 1, }),
                ListModule.OfSeq(new List<int> { 9, 1, 7, }),
                ListModule.OfSeq(new List<int> { 9, 7, 1, }),
            });

            var actualOutList = LexPerm.list(inList);

            Assert.AreEqual(expectedOutList.Length, actualOutList.Length, "lengths not equal");
            for (var i = 0; i < expectedOutList.Length; i++)
            {
                TestUtilities.AssertFSharpListsAreEqual(expectedOutList[i], actualOutList[i]);
            }
        }
    }
}
