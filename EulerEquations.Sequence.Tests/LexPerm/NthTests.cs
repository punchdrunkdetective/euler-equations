﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Microsoft.FSharp.Collections;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class LexPermTests
    {
        [Test]
        public void nthLexPerm_NegativeN_Exception()
        {
            var n = -1;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });

            Assert.Throws<Exception>(() => LexPerm.nth(n, inList));
        }

        [Test]
        public void nthLexPerm_ZeroN_Exception()
        {
            var n = 0;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });

            Assert.Throws<Exception>(() => LexPerm.nth(n, inList));
        }

        [Test]
        public void nthLexPerm_SeventhOf012_Exception()
        {
            var n = 7;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });

            Assert.Throws<Exception>(() => LexPerm.nth(n, inList));
        }

        [Test]
        public void nthLexPerm_NullList_Exception()
        {
            var n = 1;
            FSharpList<int> inList = null;

            Assert.Throws<Exception>(() => LexPerm.nth(n, inList));
        }

        [Test]
        public void nthLexPerm_EmptyListSecondPerm_Exception()
        {
            var n = 2;
            var inList = ListModule.OfSeq(new List<int>());

            Assert.Throws<Exception>(() => LexPerm.nth(n, inList));
        }

        [Test]
        public void nthLexPerm_EmptyList_EmptyList()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int>());

            var outList = LexPerm.nth(n, inList);

            Assert.IsTrue(outList.IsEmpty, "Output list not empty");
        }

        [Test]
        public void nthLexPerm_ListOfSize_OutputOfSameSize()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 1, 2, });

            var actualOutList = LexPerm.nth(n, inList);

            Assert.AreEqual(expectedOutList.Length, actualOutList.Length);
        }

        [Test]
        public void nthLexPerm_FirstOf1_1()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int> { 1, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_FirstOf01_01()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_FirstOf10_01()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int> { 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_SecondOf01_10()
        {
            var n = 2;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, 0, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_SecondOf10_10()
        {
            var n = 2;
            var inList = ListModule.OfSeq(new List<int> { 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, 0, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_FirstOf012_012()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 1, 2, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_FirstOf210_012()
        {
            var n = 1;
            var inList = ListModule.OfSeq(new List<int> { 2, 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 1, 2, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_SecondOf012_021()
        {
            var n = 2;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 2, 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_SecondOf210_021()
        {
            var n = 2;
            var inList = ListModule.OfSeq(new List<int> { 2, 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 0, 2, 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_ThirdOf012_102()
        {
            var n = 3;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, 0, 2, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_ThirdOf210_102()
        {
            var n = 3;
            var inList = ListModule.OfSeq(new List<int> { 2, 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, 0, 2, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_ForthOf012_120()
        {
            var n = 4;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, 2, 0, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_ForthOf210_120()
        {
            var n = 4;
            var inList = ListModule.OfSeq(new List<int> { 2, 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 1, 2, 0, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_FifthOf012_201()
        {
            var n = 5;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 2, 0, 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_FifthOf210_201()
        {
            var n = 5;
            var inList = ListModule.OfSeq(new List<int> { 2, 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 2, 0, 1, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_SixthOf012_210()
        {
            var n = 6;
            var inList = ListModule.OfSeq(new List<int> { 0, 1, 2, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 2, 1, 0, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }

        [Test]
        public void nthLexPerm_SixthOf210_210()
        {
            var n = 6;
            var inList = ListModule.OfSeq(new List<int> { 2, 1, 0, });
            var expectedOutList = ListModule.OfSeq(new List<int> { 2, 1, 0, });

            var actualOutList = LexPerm.nth(n, inList);

            TestUtilities.AssertFSharpListsAreEqual(expectedOutList, actualOutList);
        }
    }
}
