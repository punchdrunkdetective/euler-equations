﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class PrimeTests
    {
        [Test]
        public void List_Negative_ArgumentException()
        {
            var n = -1;
            Assert.Throws<ArgumentException>(() => Prime.list(n));
        }

        [Test]
        public void List_Zero_EmptyList()
        {
            var n = 0;

            var list = Prime.list(n);

            Assert.IsTrue(list.IsEmpty);
        }

        [Test]
        public void List_One_PrimeList()
        {
            var n = 1;
            var expectedPrimes = ListModule.OfSeq(new List<long> { 2, });

            var actualPrimes = Prime.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedPrimes, actualPrimes);
        }

        [Test]
        public void List_Five_PrimeList()
        {
            var n = 5;
            var expectedPrimes = ListModule.OfSeq(new List<long> { 2, 3, 5, 7, 11, });

            var actualPrimes = Prime.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedPrimes, actualPrimes);
        }

        [Test]
        public void List_Ten_PrimeList()
        {
            var n = 10;
            var expectedPrimes = ListModule.OfSeq(new List<long> { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, });

            var actualPrimes = Prime.list(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedPrimes, actualPrimes);
        }
    }
}
