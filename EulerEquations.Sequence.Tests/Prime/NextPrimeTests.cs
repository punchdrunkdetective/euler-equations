﻿using System;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class PrimeTests
    {
        [Test]
        public void NextPrime_Negative_ArgumentException()
        {
            var input = -1L;
            Assert.Throws<ArgumentException>(() => Prime.nextPrime(input));
        }

        [TestCase(0L, 2L)]
        [TestCase(1L, 2L)]
        [TestCase(2L, 3L)]
        [TestCase(23L, 29L)]
        [TestCase(24L, 29L)]
        public void NextPrime(long start, long next)
        {
            var actualOutput = Prime.nextPrime(start);
            Assert.AreEqual(next, actualOutput);
        }
    }
}
