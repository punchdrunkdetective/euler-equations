﻿using System;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class PrimeTests
    {
        [Test]
        public void nth_Negative_ArgumentException()
        {
            var n = -1;
            Assert.Throws<ArgumentException>(() => Prime.nth(n));
        }

        [Test]
        public void nth_Zero_FirstPrime()
        {
            var n = 0;
            var expectedPrime = 2;

            var actualPrime = Prime.nth(n);

            Assert.AreEqual(expectedPrime, actualPrime);
        }

        [Test]
        public void nth_One_SecondPrime()
        {
            var n = 1;
            var expectedPrime = 3;

            var actualPrime = Prime.nth(n);

            Assert.AreEqual(expectedPrime, actualPrime);
        }

        [Test]
        public void nth_Five_SixthPrime()
        {
            var n = 5;
            var expectedPrime = 13;

            var actualPrime = Prime.nth(n);

            Assert.AreEqual(expectedPrime, actualPrime);
        }

        [Test]
        public void nth_Ten_EleventhPrime()
        {
            var n = 10;
            var expectedPrime = 31;

            var actualPrime = Prime.nth(n);

            Assert.AreEqual(expectedPrime, actualPrime);
        }
    }
}
