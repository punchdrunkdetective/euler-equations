﻿using System;
using System.Collections.Generic;
using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    [TestFixture]
    public partial class PrimeTests
    {
        [Test]
        public void UpTo_Negative_ArgumentException()
        {
            var n = -1;
            Assert.Throws<ArgumentException>(() => Prime.upTo(n));
        }

        [Test]
        public void UpTo_Zero_EmptyList()
        {
            var n = 0;

            var actualPrimes = Prime.upTo(n);

            Assert.IsTrue(actualPrimes.IsEmpty);
        }

        [Test]
        public void UpTo_One_EmptyList()
        {
            var n = 1;

            var actualPrimes = Prime.upTo(n);

            Assert.IsTrue(actualPrimes.IsEmpty);
        }

        [Test]
        public void UpTo_Two_PrimeList()
        {
            var n = 2;
            var expectedPrimes = ListModule.OfSeq(new List<int> { 2, });

            var actualPrimes = Prime.upTo(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedPrimes, actualPrimes);
        }

        [Test]
        public void UpTo_Eight_PrimeList()
        {
            var n = 8;
            var expectedPrimes = ListModule.OfSeq(new List<int> { 2, 3, 5, 7, });

            var actualPrimes = Prime.upTo(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedPrimes, actualPrimes);
        }

        [Test]
        public void UpTo_TwentyNine_PrimeList()
        {
            var n = 29;
            var expectedPrimes = ListModule.OfSeq(new List<int> { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, });

            var actualPrimes = Prime.upTo(n);

            TestUtilities.AssertFSharpListsAreEqual(expectedPrimes, actualPrimes);
        }
    }
}
