﻿using Microsoft.FSharp.Collections;
using NUnit.Framework;

namespace EulerEquations.Sequence.Tests
{
    public static class TestUtilities
    {
        static public void AssertFSharpListsAreEqual<T>(FSharpList<T> expected, FSharpList<T> actual)
        {
            Assert.AreEqual(expected.Length, actual.Length, "Unexpected output list size.");
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }
    }
}
