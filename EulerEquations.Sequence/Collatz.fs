﻿namespace EulerEquations.Sequence

module Collatz = 

    open EulerEquations.Core
    open Validation
    
    (*
    * Generates a Collatz Sequence, represented as a list seeded at x. 
    * 
    * Collatz Sequence: starting from x, the next value will either be x / 2 if x is even 
    * or (x * 3) + 1 if x is odd till the next value is 1.
    *)
    let List (x :int64) :int64 list = 

        let isValid = 
            Ok x 
            |> Result.bind isPositive
            |> Result.bind nonZero
            
        match isValid with
        | Error e -> invalidArg "x" e
        | Ok x -> 

            let rec generate x acc = 

                let acc' = x :: acc

                match x with 
                | 1L -> List.rev acc'
                | _ ->
                    match Int64.isEven x with 
                    | true  -> generate (x / 2L) acc'
                    | false -> generate ((x * 3L) + 1L) acc'

            generate x []

