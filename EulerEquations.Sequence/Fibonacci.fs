﻿namespace EulerEquations.Sequence

module Fibonacci =

    open EulerEquations.Core
    open Validation

    (* 
    * Returns the nth Fibonacci number. Indexing is zero based. n should not be negative.
    * fib(0) = 0
    * fib(1) = 1
    * fib(2) = 1
    * 
    * O(n) 
    *)
    let nth (n :int32) :BigInteger =

        let isValid = 
            Ok n 
            |> Result.map int64
            |> Result.bind isPositive

        match isValid with
        | Error e -> invalidArg "n" e
        | Ok _ -> 
            match n with 
            | 0 -> BigInteger(0)
            | 1 -> BigInteger(1)
            | 2 -> BigInteger(1)
            | _ -> 
                let rec loop i iMinus1 iMinus2  = 
                    let newFib = BigInteger.Add(iMinus1, iMinus2)
                    match i = n with 
                    | true -> newFib
                    | false -> loop (i + 1) iMinus2 newFib
                loop 3 (BigInteger(1)) (BigInteger(1))

    (* 
    * Returns a list of the first n Fibonacci numbers. n should not be negative.
    * 
    * O(n^2) 
    *)
    let list (n :int32) :BigInteger list = 

        let isValid = 
            Ok n
            |> Result.map int64
            |> Result.bind isPositive

        match isValid with
        | Error e -> invalidArg "n" e
        | Ok _ ->
            match n = 0 with 
            | true -> []
            | false -> [0..(n-1)] |> List.map nth