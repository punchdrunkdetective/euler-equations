﻿namespace EulerEquations.Sequence

module LexPerm =

    open EulerEquations.Core
    open Validation

    (*
    * Part of Narayana Pandit's algorythm for finding lexigraphically ordered permutations.
    * Finds the largest index k in array a such that a[k] < a[k + 1].
    *)
    let private findK (a: 'T[]): int32 = 
        let l = [0..((Array.length a) - 2)]
        l |> List.fold 
            (fun k i -> 
                // When i and i+1 are decreasing in value, k will be i-1. Record that 
                // as the 'current k'.
                match a.[i] < a.[(i + 1)] with
                | true  -> i 
                | false -> k) -1 

    (*
    * Part of Narayana Pandit's algorythm for finding lexigraphically ordered permutations.
    * Find the largest index l > k in array a such that a[k] < a[l].
    *)
    let private findL (a: 'T[]) (k: int32): int32 = 
        let l = [(k + 1)..((Array.length a) - 1)]
        l |> List.fold 
            (fun l i -> 
                // If value of a[i] > a[k], record that as the 'current l'.
                match a.[k] < a.[i] with
                | true  -> i 
                | false -> l ) -1 

    (*
    * Swaps the values of a[i1] and a[i2] for array a.
    *)
    let private swap (a: 'T[]) (i1: int32) (i2: int32): unit = 
        let temp = a.[i1]
        a.[i1] <- a.[i2]
        a.[i2] <- temp

    (*
    * Reverses order of all values from a[i1] to a[i2] for array a.
    *)
    let rec private swapRange (a: 'T[]) (i1: int32) (i2: int32): unit =
        if i1 <> i2  && i1 < i2 then
            swap a i1 i2 
            swapRange a (i1 + 1) (i2 - 1)

    (*
    * Returns the nth lexigraphically ordered permutation of a numeric list.
    *)
    let nth (n: int32) (l: 'T list): 'T list = 

        let isValid = 
            Ok (n, l)
            |> Result.map (fun _ -> int64 n)
            |> Result.bind isPositive
            |> Result.bind nonZero
            |> Result.map (fun _ -> l)
            |> Result.bind nonNull
            |> Result.map (fun _ -> BigInteger(n), List.length l |> int64)
            |> Result.bind (fun (n, listLength) -> 
                match n > Int64.factorial listLength with
                | true -> Error (System.String.Format("{0}th permutation does not exist.", n))
                | false -> Ok (n, listLength))
            |> Result.map (fun _ -> n, l)

        match isValid with
        | Error e -> failwith e
        | Ok _ -> 
        
            // This will use an algorythm derived by Narayana Pandit's.
            let a = Array.ofList l |> Array.sort

            let rec loop i = 
                if i < n then
                    let k = findK a
                    let l = findL a k
                    swap a k l
                    swapRange a (k + 1) ((Array.length a) - 1)
                    loop (i + 1)
            loop 1

            List.ofArray a
    
    (*
    * Returns lexigraphically ordered permutations of the given numeric list. Note, you should probably make sure 
    * this is feasable. Function is not garranteed to terminate if the total list of permuations are large. 
    *)
    let list (l: 'T list): 'T list list =

        let isValid = 
            Ok l 
            |> Result.bind nonNull

        match isValid with
        | Error e -> invalidArg "l" e
        | Ok _ ->

            match List.length l = 0 with
            | true -> []
            | false ->

                let a = Array.ofList l |> Array.sort
                let n = Int64.factorial (int64 (List.length l))

                let rec loop perms i = 
                    match i >= n with
                    | true -> perms
                    | false ->
                    
                        // i is only used for looping. The following simply needs to be done i times.

                        let k = findK a
                        let l = findL a k
                        swap a k l
                        swapRange a (k + 1) ((Array.length a) - 1)
                        
                        loop ((List.ofArray a) :: perms) (i + BigInteger(1))                        

                (loop [(List.ofArray a)] (BigInteger(1))) |> List.rev
