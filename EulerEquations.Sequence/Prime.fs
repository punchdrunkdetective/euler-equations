﻿namespace EulerEquations.Sequence

module Prime = 

    open EulerEquations.Core
    open Validation

    (*
    * From a known value, determine the next prime.
    *
    * O( p * sqrt(2p) )
    *)
    let nextPrime (p :int64) :int64 = 

        // Analaysis: 
        //
        // Observe main loop is O(p' - p)
        // Observe critiacl operation is O(sqrt(p'))
        //
        // O( ('p - p) sqrt(p') )
        //
        // Observe p' < 2p, therfore ('p - p) <= p 
        //
        // O( p * sqrt(2p) )
        
        let isValid = 
            Ok p
            |> Result.bind isPositive

        match isValid with 
        | Error e -> invalidArg "p" e
        | Ok p -> 
        
            // Starting at p, loop till you find a prime number.
            let rec loop (i :int64) :int64 = 
        
                let i' = (i + 1L)
                match not (Int64.isFactorable i') && i' <> 1L with 
                | true -> i'
                | false -> loop i' // As long as the set of primes if infinite, we will always terminate.

            loop (int64 p)

    (* 
    * Gives you the nth prime number. Indexing is zero based. n should not be negative.
    * 
    * O( pn * sqrt(pn) )
    *)
    let nth (n :int32) :int64 = 

        // Observe main loop is O(n).
        // Observe critical operation is O( pi * sqrt(2pi) ) where pi is the current prime in the loop.
        // Observe that the main loop terminates on pn where pn is the nth prime.

        // O( n * (pn * sprt(2pn)) )

        // Observer that the critical operation iterates over ever i in p to p' (implementation of nextPrime)
        // Observer p0 = 2
        // Observe that we are really iterating over 2 to pn, with critical operatio being <= sqrt(pn) (implementation of nextPrime)

        // O( pn * sqrt(2pn)  )

        // FOLLOWING NEEDS PROOF READING
        // Observe main loop is O(n).
        // Observe critical operation is O( pi * sqrt(2pi) ) where pi is the current prime in the loop.
        // Observe that the main loop terminates on pn where pn is the nth prime.
        // Observe O( pi * sqrt(2pi) ) <= O( pn * sqrt(2pn) )
        //
        // O( n * (pn * sprt(2pn)) )
        //
        // Observer the following:
        // p' < 2p, therfore:
        // p1 < 2 p0 -> p1 < 2^1 p0
        // p2 < 2 p1 -> p2 < 2 * (2 p0) -> p2 < 2^2 p0
        // p3 < 2 p2 -> p3 < 2 * (2 p2) -> p3 < 2 * 2 * (2 p0) -> p3 < 2^3 p0
        // ...
        // pn < 2 pn-1 -> pn < 2^n p0
        // Observer p0 = 2^1, therefore pn < 2 * 2^n
        //
        // O((2^n * sqrt(2^n)) ) 

        let isValid = 
            Ok n 
            |> Result.map int64
            |> Result.bind isPositive

        match isValid with
        | Error e -> invalidArg "n" e
        | Ok _ -> 
        
            let n' = (n + 1) // Adjust to one base indexing for easy counting.

            // Loop to next prime till you have found (n + 1) primes.
            let rec loop (p :int64) (primesFound :int32) :int64 =             
                match primesFound = n' with
                | true -> p
                | false -> loop (nextPrime p) (primesFound + 1)            
            loop 2L 1

    (* 
    * Gives you a list of the first n prime numbers. n should not be negative.
    * 
    * O( n * (pn * sqrt(pn)) )
    *)
    let list (n :int32) :int64 list = 

        let isValid = 
            Ok n
            |> Result.map int64
            |> Result.bind isPositive

        match isValid with
        | Error e -> invalidArg "n" e
        | Ok _ -> 
            match n = 0 with 
            | true -> []
            | false ->[0..(n - 1)] |> List.map(nth)

    (*
    * Returns a seive of prime number no greater than x. x should not be negative.
    * 
    * O(x)
    *)
    let upTo (x :int32) :int32 list = 

        let isValid = 
            Ok x
            |> Result.map int64
            |> Result.bind isPositive

        match isValid with
        | Error e -> invalidArg "x" e
        | Ok _ -> 

            match x with
            | 0 | 1 -> [] 
            | _ ->
        
                let potentialPrimes = 2 :: [3..2..x] // Start with only odd numbers and 2.

                let rec sieve primeAcc l = 
                    match l with 
                    | [] -> List.rev primeAcc
                    | p :: ps ->
                
                        let nonMultipleOfP i = (i % p) > 0

                        // Fist item is assumed and must be a valid prime due to the filtering done on ps.
                        let primeAcc' = p :: primeAcc
                        let l' = (List.filter nonMultipleOfP ps)

                        l' |> sieve primeAcc'

                potentialPrimes |> sieve []

