﻿namespace EulerEquations.Sequence

module Validation =

    let isPositive x =  
        match x < 0L with
        | true -> Error (sprintf "negative value: %i" x)
        | false -> Ok x

    let nonZero x = 
        match x = 0L with 
        | true -> Error "zero value"
        | false -> Ok x

    let nonNull o =
        match isNull (box o) with
        | true -> Error "null object"
        | false -> Ok o